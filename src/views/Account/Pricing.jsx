import React, { Component } from "react";
import { Grid, Row, Col, Table } from "react-bootstrap";
import Button from "../../components/CustomButton/CustomButton.jsx";
import { getPlans, getCurrentPlan } from "../../utils/commons";
import NotificationManager from "../../utils/Notifications";

import { API_URL } from "../../utils/constants";
import logo from "../../assets/img/logo.png";
import axios from "axios";

import Card from "../../components/Card/Card.jsx";

class RegularForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: "1",
      radioVariant: "1",
      plans: {}
    };
  }
  componentDidMount() {
    getPlans(this);
    getCurrentPlan(this);
  }
  openCheckout = (event, plan) => {
    const self = this;
    const access_token = localStorage.getItem("access_token");
    const amount = plan.price;
    const headers = {
      "x-access-token": access_token
    };
    let options = {
      key: "rzp_test_yiPAURdEF8q4d4",
      amount: amount, // 2000 paise = INR 20, amount in paisa
      name: "Visitor Management",
      description: "Pay for your subscription",
      image: logo,
      handler: function(response) {
        axios
          .post(
            `${API_URL}/subscription`,
            {
              paymentId: response.razorpay_payment_id,
              planId: plan.id,
              amount
            },
            { headers }
          )
          .then(respone => {
            NotificationManager.success(self, "Payment Completed!.");
            getCurrentPlan(self);
          })
          .catch(e => {
            NotificationManager.error(self, "Payment Failed!.");
          });
      },
      theme: {
        color: "green"
      }
    };
    const rzp = new window.Razorpay(options);
    rzp.open();
  };

  handleRadio = event => {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  };
  render() {
    const { plans } = this.state;
    return plans.length ? (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12} >
              <Card 
              hCenter
                title="Active Plan"
                content={
                  <Table responsive striped condensed hover>
                    <thead>
                      <tr>
                        <th>Plan</th>
                        <th>Subscribed On</th>
                        <th>Expires On</th>
                        <th>Employee Limit</th>
                        <th>Device Limit</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{this.state.currentPlan}</td>
                        <td>
                          {new Date(this.state.subscribedOn).toDateString()}
                        </td>
                        <td>{new Date(this.state.expiresOn).toDateString()}</td>
                        <td>{this.state.employeeLimit}</td>
                        <td>
                          {this.state.deviceLimit > 100
                            ? "Unlimited"
                            : this.state.deviceLimit}
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                }
              />
              <Row>
                {plans.map(plan => {
                  console.log(plan);
                  return plan.name !== "Trial" ? (
                    <Col key={plan.id} xs={12} md={3}>
                      <Card
                        title={plan.name}
                        content={
                          <div>
                            Employee Limit:{" "}
                            <strong> {plan.employeeLimit}</strong> <br/>
                            Device Limit:{" "}
                            <strong>
                              {" "}
                              {plan.deviceLimit > 100
                                ? "Unlimited"
                                : plan.deviceLimit}
                            </strong> <br/>
                            Validity: <strong> {plan.validity}</strong> <br/>
                            Price: <strong> ₹{plan.price / 100}</strong> <br/>
                            Support: <strong> Mon-Fri(24/7) </strong> <br/>
                            <hr />
                            <Button
                              value={plan.price}
                              bsStyle="success"
                              bsSize="lg"
                              fill
                              ctTableFullWidth
                              disabled={
                                plan.name === this.state.currentPlan
                                  ? true
                                  : false
                              }
                              onClick={e => this.openCheckout(e, plan)}
                            >
                              {plan.name === this.state.currentPlan
                                  ?' Active Plan ': 'Purchase Plan'}
                            </Button>
                          </div>
                        }
                      />
                    </Col>
                  ) : (
                    ""
                  );
                })}
              </Row>
            </Col>
          </Row>
        </Grid>
      </div>
    ) : (
      <span> Loading ... </span>
    );
  }
}

export default RegularForms;
