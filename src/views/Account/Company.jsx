import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";

import { API_URL } from "../../utils/constants"

import Card from "../../components/Card/Card.jsx";

import Button from "../../components/CustomButton/CustomButton.jsx";

import axios from "axios";

import NotificationManager from "../../utils/Notifications"

class RegularForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: "1",
      radioVariant: "1",
      companyName: ''
    };
  }
  handleRadio = event => {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  };

  handleSubmit = event => {
    const self = this;
    !this.state.companyName 
    ? NotificationManager.error(self, 'Enter company name')
    : (()=> {
        axios.post(`${API_URL}/auth/change-company-name`, {companyName: self.state.companyName}, {headers: {
          'x-access-token' : localStorage.getItem('access_token')
        }})
        .then(response => {
          localStorage.setItem('companyName', self.state.companyName);
          NotificationManager.success(self, 'Company name updated');
        })
        .catch(err => {
          NotificationManager.error(self, 'Could not update company name')
        })
    })() 
  }
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Update Company Details"
                content={
                  <FormGroup>
                    <ControlLabel>Company Name</ControlLabel>
                    <FormControl placeholder={localStorage.getItem("companyName") ? localStorage.getItem("companyName") : "Tesla"} type="text"
                    value={this.state.companyName} 
                    onChange= {e => {
                      this.setState({
                        companyName: e.target.value
                      })
                    }}/>
                    <br />
                    <Button bsStyle="info" fill onClick={this.handleSubmit} >
                      Update
                    </Button>
                  </FormGroup>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default RegularForms;
