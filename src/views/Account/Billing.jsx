import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import ReactTable from "react-table";
import axios from 'axios';

import Card from "../../components/Card/Card.jsx";

// import Button from "../../components/CustomButton/CustomButton.jsx";
import { API_URL } from "../../utils/constants";
import NotificationManager from '../../utils/Notifications';

class RegularForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: "1",
      radioVariant: "1",
      data: []
    };
  }

  componentDidMount(){
    this.getPurchaseHistory()
  }

  getPurchaseHistory = () => {
    const self = this;
    axios.get(`${API_URL}/subscription/purchase-history`, {
      headers: {
        'x-access-token' : localStorage.getItem('access_token')
      }
    })
    .then(response  => {
      console.log(response.data.data);
      self.setState({
        data: response.data.data
      })
    })
    .catch(err => NotificationManager.error(self,'Failed to fetch payment history'));
    
  }

  handleRadio = event => {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  };
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12} className={"superTable"}>
            {console.log(this.state.data)}
              <Card
                title="Billing Transactions"
               
                content={
                  <ReactTable
                    data={this.state.data}
                    
                    filterable
                    columns={[
                      {
                        Header: "Plan Name",
                        accessor: "plan.name",
                        filterable: false
                      },
                      {
                        id: 'id',
                        Header: "Date",
                        accessor: d => {
                          return new Date(d.createdAt).toDateString();
                        },
                        filterable: false
                      },
                      {
                        id: 'plan.id',
                        Header: "Amount",
                        accessor: d => {
                          return `₹${d.plan.price/100}`
                        },
                        sortable: false,
                        filterable: false
                      },
                      {
                        id: 'index',
                        Header: "Expiry",
                        accessor: d => {
                          if(d.createdAt && d.plan.validity){
                            const date = new Date(d.createdAt);
                          const expiry = new Date(date.setDate(date.getDate()+d.plan.validity))
                          return expiry.toDateString()
                          } else return 'Not Available'
                        },
                        sortable: false,
                        filterable: false
                      }
                    ]}
                    defaultPageSize={10}
                    showPaginationBottom={true}
                    className="-striped -highlight"
                  />
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}



export default RegularForms;
