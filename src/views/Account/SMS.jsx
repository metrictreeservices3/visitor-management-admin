import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import ReactTable from "react-table";

import Card from "../../components/Card/Card.jsx";

import Button from "../../components/CustomButton/CustomButton.jsx";

class RegularForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: "1",
      radioVariant: "1",
      data: [
        {
          cname: "LmntrX",
          success: "Yes",
          date: "4 July 2018",
          cardno: "2000-XXX-XXX-XXX",
          amount: "10,000 INR",
          receipt: (
            <div className="actions-right">
              <Button bsStyle="info">View Receipt</Button>
            </div>
          )
        },
        {
          cname: "Tesla",
          success: "No",
          date: "4 July 1996",
          cardno: "-",
          amount: "0 INR",
          receipt: (
            <div className="actions-right">
              <Button bsStyle="info">View Receipt</Button>
            </div>
          )
        }
      ]
    };
  }
  handleRadio = event => {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  };
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="SMS Transactions"
                content={
                  <ReactTable
                    data={this.state.data}
                    filterable
                    columns={[
                      {
                        Header: "Company Name",
                        accessor: "cname",
                        filterable: false
                      },
                      {
                        Header: "Success",
                        accessor: "success",
                        filterable: false
                      },
                      {
                        Header: "Date",
                        accessor: "date",
                        filterable: false
                      },
                      {
                        Header: "Card Number",
                        accessor: "cardno",
                        sortable: false,
                        filterable: false
                      },
                      {
                        Header: "Amount",
                        accessor: "amount",
                        sortable: false,
                        filterable: false
                      },
                      {
                        Header: "Receipt",
                        accessor: "receipt",
                        sortable: false,
                        filterable: false
                      }
                    ]}
                    defaultPageSize={10}
                    showPaginationBottom={true}
                    className="-striped -highlight"
                  />
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default RegularForms;
