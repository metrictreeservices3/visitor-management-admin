import React, { Component } from "react";
import ReactTable from "react-table";
import { Grid, Row, Col, Nav, NavItem, Tab , Button,Carousel ,Modal,} from "react-bootstrap";
import { Link } from 'react-router-dom'
// import Button from "../../components/CustomButton/CustomButton.jsx";

import Card from "../../components/Card/Card.jsx";
import { logout, getData } from "../../utils/commons";
// import Button from 'react-bootstrap/Button';
// import modal1 from "../../assets/img/modal.jpg";
// import modal2 from "../../assets/img/modal.jpg";
// import modal3 from "../../assets/img/modal.jpg"
import {
  VISITOR_TIMELINE_TABLE_COLUMNS,
  EMPLOYEE_TIMELINE_TABLE_COLUMNS
} from "../../utils/constants";
import image1 from '../../assets/img/01.png';
import image2 from '../../assets/img/02.png';
import image3 from '../../assets/img/03.png';
import  '../../assets/css/carousel.css' 

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = {
      is_signingout: false,
      signOutText: "Sign Out",
      employees_in: 0,
      employees_out: 0,
      visitors_in: 0,
      pre_registered: 0,
      tutorialModal:false
    };
  }

  componentDidMount() {
    getData(this);
  }

  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title={
                  <div className="clearfix">
                    <span>
                      {localStorage.getItem("companyName")
                        ? localStorage.getItem("companyName").trim()
                        : logout()}{" "}
                      <i className="fa fa-building-o" />

                    </span>
                  <Button className="pull-right" onClick={()=>this.setState({tutorialModal:!this.state.tutorialModal})}>How To Start?</Button>
                  </div>
                }
                content={
                  <Row>
                    <Col md={12}>
                      <Col md={3}>
                        <h2 style={{ color: "#447DF7" }}>
                          {this.state.employees_in}{" "}
                          <i className="fa fa-user-plus sidebar-nav-icon" />
                        </h2>
                        Employees in
                      </Col>
                      <Col md={3}>
                        <h2 style={{ color: "#87CB16" }}>
                          {this.state.employees_out}{" "}
                          <i className="fa fa-user-times sidebar-nav-icon" />
                        </h2>
                        Employees out
                      </Col>
                      <Col md={3}>
                        <h2 style={{ color: "#23CCEF" }}>
                          {this.state.visitors_in}{" "}
                          <i className="fa fa-address-card" />
                        </h2>
                        Visitors in
                      </Col>
                      <Col md={3}>
                        <h2 style={{ color: "#FB404B" }}>
                          {this.state.pre_registered}{" "}
                          <i className="fa fa-user" />
                        </h2>
                        Pre-registered
                      </Col>
                    </Col>
                  </Row>
                }
              />
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <Card
                title="Timeline"
                content={
                  <Tab.Container
                    id="tabs-with-dropdown"
                    defaultActiveKey="employees"
                  >
                    <Row className="clearfix">
                      <Col sm={12}>
                        <Nav bsStyle="tabs">
                          <NavItem eventKey="employees">Employees</NavItem>
                          <NavItem eventKey="visitors">Visitors</NavItem>
                        </Nav>
                      </Col>
                      <Col sm={12}>
                        <Tab.Content animation>
                          <Tab.Pane eventKey="employees">
                            <ReactTable
                              data={
                                this.state.employeesData
                                  ? this.state.employeesData
                                  : []
                              }
                              filterable
                              columns={EMPLOYEE_TIMELINE_TABLE_COLUMNS}
                              defaultPageSize={10}
                              showPaginationBottom={true}
                              className="-striped -highlight"
                            />
                          </Tab.Pane>
                          <Tab.Pane eventKey="visitors">
                            <ReactTable
                              data={
                                this.state.visitorsData
                                  ? this.state.visitorsData
                                  : []
                              }
                              filterable
                              columns={VISITOR_TIMELINE_TABLE_COLUMNS}
                              defaultPageSize={10}
                              showPaginationBottom={true}
                              className="-striped -highlight"
                            />
                          </Tab.Pane>
                        </Tab.Content>
                      </Col>
                    </Row>
                  </Tab.Container>
                }
              />
            </Col>
          </Row>
       
        <Modal show={this.state.tutorialModal} size='xl' onHide={()=>this.setState({tutorialModal:!this.state.tutorialModal})}>
        <Modal.Body>
        <Carousel nextIcon={"Next>>"} nextLabel="Next" prevIcon={"<<Prev"}>
  <Carousel.Item>
             
                <Row>
                <Col  sm={6}>
                  <h4 className="step-name">step 01</h4>
                  <h4 className="task-name">iPad Configuration</h4>
                  
                  <h5>Goto Settings > iPad</h5>
                  <ul>
                    <li>add iPad and location</li>
                    <li>use this DEVICE ID to complte iPad app installation process</li>
                  </ul>
                  <Button className="car-btn btn-primary btn-fill"  href="#" size="sm" variant="primary">Click here to download app</Button>
                  </Col>
                  <Col sm={6}>
                  <img
      className="d-block w-100 car"
      src={image1}
      alt="First slide"
    />
                  </Col>
                  
                </Row>
            

  </Carousel.Item>
  <Carousel.Item>
  <Row>
  <Col  sm={6}>
                  <h4 className="step-name">step 02</h4>
                  <h4 className="task-name">Add Employee</h4>
                  <br></br>
                  <h5>Employees > Add Employee</h5>
                  <ul>
                    <li>Enter the details and press Add Employee button</li>
                  </ul>
                  <Link
                  to={{
                    pathname: "/employees",
                    state: {
                      showModalOnNav: true
                    }
                  }} 
                 
                  >
                  <Button className="car-btn btn-primary btn-fill" size="sm" variant="primary">Click here to add employee</Button>
                  </Link>                  
                  </Col>
                  <Col sm={6}>
                  <img
      className="d-block w-100 car"
      src={image2}
      alt="First slide"
    />
                  </Col>
                  
                </Row>
  </Carousel.Item>
  <Carousel.Item>
  <Row>
                <Col  sm={6}>
                  <h4 className="step-name">step 03</h4>
                  <h4 className="task-name">Mark Check-in</h4>
                  <br></br>
                  <h5>Start using the app to mark visit</h5>
                  <ul>
                    <li>Press employee button for swaping to employee check in option </li>
                    <li>Press Visitor in button to enter in and Visitor out to signout the visitor</li>
                  </ul>
                  </Col>
                  <Col sm={6}>
                  <img
      className="d-block w-100 car"
      src={image3}
      alt="First slide"
    />
                  </Col>
                  
                </Row>
  </Carousel.Item>
</Carousel>
</Modal.Body>
       
      </Modal> 

        
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
