import React, { Component } from 'react';
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock,
  Form,
  InputGroup
} from 'react-bootstrap';
import Select from 'react-select';
import { selectOptions } from '../variables/Variables.jsx';

import Card from 'components/Card/Card.jsx';

import Checkbox from '../components/CustomCheckbox/CustomCheckbox.jsx';
import Button from '../components/CustomButton/CustomButton.jsx';
import Radio from '../components/CustomRadio/CustomRadio.jsx';

class RegularForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: '1',
      radioVariant: '1'
    };
  }
  handleRadio = event => {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  };
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card title="Settings" />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default RegularForms;
