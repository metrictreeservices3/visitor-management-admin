import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";

import Creatable from "react-select/lib/Creatable";
import Select from "react-select";

import Card from "../components/Card/Card.jsx";

import Button from "../components/CustomButton/CustomButton.jsx";
import axios from "axios";
import { API_URL } from "../utils/constants";
import {
  handleError,
  getEmployeesList,
  getVisitorsList,
  visitorDataIsNotValid
} from "../utils/commons";

import NotificationManager from "../utils/Notifications";

class RegularForms extends Component {
  state = {};
  componentWillMount() {
    getEmployeesList(this);
    getVisitorsList(this);
  }
  
  submit() {
    if (visitorDataIsNotValid(this)) return;
    let loader = NotificationManager.ongoing(
      this,
      `Signing in ${this.state.name}`,
      "spinner"
    );

    const formData = new FormData();
    const access_token = localStorage.getItem("access_token");
    let url = this.state.selectedId
      ? `${API_URL}/session/checkin/${this.state.selectedId}`
      : `${API_URL}/visitor/add_session`;
    if (this.state.selectedId)
      formData.append("host", this.state.selectedEmployeeId);
    else {
      formData.append("email", this.state.email);
      formData.append("name", this.state.name);
      formData.append("location", this.state.location);
      formData.append("companyName", this.state.companyName);
      formData.append("host", this.state.selectedEmployeeId);
      formData.append("adminCompany", localStorage.getItem('companyName'));
    }

    axios
      .post(url, formData, {
        headers: {
          "x-access-token": access_token,
          "content-type": "multipart/form-data"
        }
      })
      .then(response => {
        NotificationManager.removeNotification(this, loader);
        window.location.assign("#/dashboard");
      })
      .catch(error => {
        NotificationManager.removeNotification(this, loader);
        NotificationManager.error(this, handleError(error));
      });
  }
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Sign in visitor"
                content={
                  <form>
                    <FormGroup>
                      <ControlLabel>
                      Full name
                        <span className="star">*</span>
                      </ControlLabel>
                      <FormControl
                        placeholder="Tim Cook"
                        type="text"
                        onChange={e => {
                          this.setState({
                            nameError: null,
                            name: e.target.value
                          });
                        }}
                        value={this.state.name ? this.state.name : ""}
                      />
                      {this.state.nameError}
                    </FormGroup>
                    
                    
                    
                    <FormGroup>
                      <ControlLabel>Company Name</ControlLabel>
                      <FormControl
                        placeholder="Tesla"
                        type="text"
                        onChange={e => {
                          this.setState({
                            companyName: e.target.value
                          });
                        }}
                        value={
                          this.state.companyName ? this.state.companyName : ""
                        }
                      />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>
                        Email
                        <span className="star">*</span>
                      </ControlLabel>
                      <FormControl
                        placeholder="elon@tesla.com"
                        type="text"
                        onChange={e => {
                          this.setState({
                            emailError: null,
                            email: e.target.value
                          });
                        }}
                        value={this.state.email ? this.state.email : ""}
                      />
                      {this.state.emailError}
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Location</ControlLabel>
                      <FormControl
                        placeholder="New York, USA"
                        type="text"
                        onChange={e => {
                          this.setState({
                            location: e.target.value
                          });
                        }}
                        value={this.state.location ? this.state.location : ""}
                      />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>
                        Visiting
                        <span className="star">*</span>
                      </ControlLabel>
                      <Select
                        placeholder="Tim Cook"
                        name="employeeSelect"
                        value={
                          this.state.employeeSelect
                            ? this.state.employeeSelect
                            : ""
                        }
                        options={
                          this.state.employeesList
                            ? this.state.employeesList
                            : []
                        }
                        defaultValue={
                          this.state.employeesList
                            ? this.state.employeesList[0]
                              ? this.state.employeesList[0]
                              : ""
                            : ""
                        }
                        onChange={employee => {
                          this.setState({
                            hostError: null,
                            employeeSelect: employee,
                            selectedEmployeeId: employee ? employee.value : null
                          });
                        }}
                      />
                      {this.state.hostError}
                    </FormGroup>

                    {/* <FormGroup>
                      <Checkbox
                        isChecked
                        number="1"
                        label="Remember Guest Details"
                      />
                    </FormGroup> */}
                    <Button
                      bsStyle="info"
                      fill
                      onClick={() => {
                        this.submit();
                      }}
                    >
                      Submit
                    </Button>
                  </form>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default RegularForms;
