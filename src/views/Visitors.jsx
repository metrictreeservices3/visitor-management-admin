import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col } from "react-bootstrap";
import PrintJs from 'print-js'
import Card from "../components/Card/Card.jsx";
import Button from "../components/CustomButton/CustomButton.jsx";
import { getData } from "../utils/commons";
import { VISITOR_TIMELINE_TABLE_COLUMNS } from "../utils/constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPrint, faFileCsv } from "@fortawesome/free-solid-svg-icons";

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = {
      is_signingout: false,
      signOutText: "Sign Out"
    };
  }

  componentWillMount() {
    getData(this);
  }
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title={
                  <Row>
                    <Col md={6}>
                      <h3>Timeline</h3>
                    </Col>
                    <Col md={5}>
                      <Row>
                        {/* <Button bsStyle="success" bsSize="sm" fill wd>
                          Add Visitor
                        </Button>
                        &nbsp; */}
                        {/* <Button bsStyle="danger" bsSize="sm" fill wd>
                          Evacuate All
                        </Button>{" "}
                        &nbsp; */}
                      </Row>
                    </Col>
                    <Col md={1}>
                      <FontAwesomeIcon 
                      className="action-icon"
                      icon={faPrint}
                      onClick={() => {
                        console.log(this.state.visitorsData)
                       const data = this.state.visitorsData.map((emp, index) => {
                        return {
                          id: index+1, 
                        name: emp.name, 
                        date: emp.date,
                        checkInTime: emp.checkInTime,
                        checkOutTime: emp.checkOutTime,
                        companyName:emp.company
                        }
                       })
                        PrintJs({printable: data, type: 'json', properties: ['id', 'name', 'date','checkInTime','checkOutTime','companyName']})
                      }}
                      />
                      &nbsp; &nbsp;
                      {/* <FontAwesomeIcon
                        className="action-icon"
                        icon={faFileCsv}
                      />{" "} */}
                    </Col>
                  </Row>
                }
                content={
                  <ReactTable
                    data={
                      this.state.visitorsData ? this.state.visitorsData : []
                    }
                    filterable
                    columns={VISITOR_TIMELINE_TABLE_COLUMNS}
                    defaultPageSize={10}
                    showPaginationBottom={true}
                    className="-striped -highlight"
                  />
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
