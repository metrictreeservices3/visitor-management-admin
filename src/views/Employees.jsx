import React, { Component } from "react";
import { Redirect } from "react-router-dom"
// react component for creating dynamic tables
import ReactTable from "react-table";
import PrintJs from 'print-js'
import {
  Modal,
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl,
  Tooltip,
  Nav,
  NavItem,
  Tab,
  Label,
  OverlayTrigger
} from "react-bootstrap";

import Card from "../components/Card/Card.jsx";
import Button from "../components/CustomButton/CustomButton.jsx";
import {
  getData,
  handleError,
  employeeDataIsNotValid,
  getSignInButton,
  evacuate,
  printTable

} from "../utils/commons";
import {
  API_URL,
  COMMON_HEADER,
  EMPLOYEES_TABLE_COLUMNS,
  EMPLOYEE_TIMELINE_TABLE_COLUMNS
} from "../utils/constants";
import axios from "axios";
import NotificationManager from "../utils/Notifications";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPrint, faFileCsv } from "@fortawesome/free-solid-svg-icons";

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = {
      is_signingout: false,
      signOutText: "Sign Out",
      showModal: false,
      navModalCtrl: false,
      showEvacuateModal: false
    };
  }
  componentDidMount() {
    
    this.showModalOnNav(this)

    getData(this, () => {
      this.getEmployeesList();
    });
  }
  getEmployeesList() {
    let access_token = localStorage.getItem("access_token");
    axios
      .get(`${API_URL}/employee/`, COMMON_HEADER(access_token))
      .then(response => {
        console.log(response.data["users"].length)
        this.setState({
          listData: response.data.users.reverse().map(employee => {
            return {
              id: employee.id,
              name: employee.name,
              email: employee.email,
              phone: employee.phone,
              location: employee.location,
              role: employee.role,
              actions: getSignInButton(employee.id, this, () => {
                this.setState({
                  activeTab: "timeline"
                });
              })
            };
          }),
          employeesRemaining: (response.data.limit - response.data.users.length)+1
        });
      })
      .catch(error => {
        NotificationManager.error(this, handleError(error));
      });
  }
  addEmployee() {
    if (employeeDataIsNotValid(this)) return;
    this.setState({ showModal: false });
    let loader = NotificationManager.ongoing(
      this,
      "Adding Employee.",
      "spinner"
    );
    let access_token = localStorage.getItem("access_token");
    axios
      .post(
        `${API_URL}/employee/add`,
        {
          email: this.state.email,
          name: this.state.name,
          role: this.state.role,
          phone: this.state.phone,
          location: this.state.location,
          adminCompany:  localStorage.getItem('companyName')
        },
        COMMON_HEADER(access_token)
      )
      .then(response => {
        this.getEmployeesList();
        NotificationManager.removeNotification(this, loader);
        NotificationManager.success(this, "Employee added.");
        this.setState({
          email: "",
          name: "",
          role: "",
          phone: "",
          location: ""
        });
      })
      .catch(error => {
        NotificationManager.removeNotification(this, loader);
        NotificationManager.error(this, "Could not add employee.");
        NotificationManager.error(this, handleError(error));
      });
  }

  showModalOnNav (ctx) {
    if(ctx.props && ctx.props.location && ctx.props.location.state && ctx.props.location.state.showModalOnNav) {
      ctx.setState({
        showModal: true,
      })
    }
  }
 
  removePost = <Tooltip id="remove">Remove Photo</Tooltip>;
  render() {
    const tooltip = (
      <Tooltip id="tooltip">
        <strong>{this.state.employeesRemaining}</strong> employees remaining!
      </Tooltip>
    );
    if(this.state.navModalCtrl) {
      return <Redirect to="/dashboard" />
    }
    
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title={
                  <Row>
                    <Col md={6}>
                      <h3>Timeline & List</h3>
                    </Col>
                    <Col md={5}>
                      <Row>
                        <Modal
                          show={this.state.showModal}
                          onHide={() => {
                            const stateObj = {
                              showModal: false
                            };

                            if(this.props 
                              && this.props.location 
                              && this.props.location.state 
                              && this.props.location.state.showModalOnNav ) {
                                stateObj.navModalCtrl = true
                              }
                            this.setState(stateObj);
                             
                          }}
                        >
                          <Modal.Header closeButton>
                            <Modal.Title>Add Employee</Modal.Title>
                          </Modal.Header>
                          <Modal.Body>
                            <form>
                              <FormGroup>
                                <ControlLabel>
                                  Full name
                                  <span className="star">*</span>
                                </ControlLabel>
                                <FormControl
                                  placeholder="Tim Cook"
                                  type="text"
                                  value={this.state.name ? this.state.name : ""}
                                  onChange={e => {
                                    this.setState({
                                      name: e.target.value
                                    });
                                  }}
                                />
                                {this.state.nameError}
                              </FormGroup>
                              <FormGroup>
                                <ControlLabel>Role</ControlLabel>
                                <FormControl
                                  placeholder="Senior Emoji Developer"
                                  type="text"
                                  onChange={e => {
                                    this.setState({
                                      role: e.target.value
                                    });
                                  }}
                                  value={this.state.role ? this.state.role : ""}
                                />
                              </FormGroup>
                              <FormGroup>
                                <ControlLabel>Phone</ControlLabel>
                                <FormControl
                                  placeholder="+91 7012549278"
                                  type="phone"
                                  onChange={e => {
                                    this.setState({
                                      phone: e.target.value
                                    });
                                  }}
                                  value={
                                    this.state.phone ? this.state.phone : ""
                                  }
                                />
                              </FormGroup>
                              <FormGroup>
                                <ControlLabel>
                                  Email
                                  <span className="star">*</span>
                                </ControlLabel>
                                <FormControl
                                  placeholder="elon@tesla.com"
                                  type="email"
                                  onChange={e => {
                                    this.setState({
                                      emailError: null,
                                      email: e.target.value
                                    });
                                  }}
                                  value={
                                    this.state.email ? this.state.email : ""
                                  }
                                />
                                {this.state.emailError}
                              </FormGroup>
                              <FormGroup>
                                <ControlLabel>Location</ControlLabel>
                                <FormControl
                                  placeholder="New York, USA"
                                  type="text"
                                  onChange={e => {
                                    this.setState({
                                      location: e.target.value
                                    });
                                  }}
                                  value={
                                    this.state.location
                                      ? this.state.location
                                      : ""
                                  }
                                />
                              </FormGroup>
                            </form>
                          </Modal.Body>
                          <Modal.Footer>
                            <Button
                              bsStyle="success"
                              fill
                              onClick={() => {
                                this.addEmployee();
                              }}
                            >
                              Add Employee
                            </Button>
                          </Modal.Footer>
                        </Modal>

                        <Modal
        show={this.state.showEvacuateModal}
        onHide={() => this.setState({ showEvacuateModal: false })}
      >
        <Modal.Header closeButton>
          <Modal.Title>Evacuate All Employees</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure you want to signout all employees? This action cannot be
          undone.
        </Modal.Body>
        <Modal.Footer>
          <Button
            bsStyle="danger"
            fill
            onClick={() => {
              this.setState({ showEvacuateModal: false });
              evacuate(this, "employee", () => {
                window.location.reload();
              });
            }}
          >
            Yes
          </Button>
          <Button
            bsStyle="warning"
            fill
            onClick={() => {
              this.setState({ showEvacuateModal: false });
            }}
          >
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>

                        <OverlayTrigger placement="bottom" overlay={tooltip}>
                          <Label bsSize="small">{this.state.employeesRemaining}</Label>
                        </OverlayTrigger>
                        &nbsp;
                        <Button
                          bsStyle="success"
                          bsSize="sm"
                          fill
                          wd
                          onClick={() => this.setState({ showModal: true })}
                        >
                          Add Employee
                        </Button>

                        &nbsp;
                        <Button
                          bsStyle="danger"
                          bsSize="sm"
                          fill
                          wd
                          // onClick={() => 
                          //   this.setState({ showEvacuateModal: true })
                          // }
                          onClick={() => this.setState({ showEvacuateModal: true })}
                        >
                          Evacuate All
                        </Button>{" "}
                        {/* <Button onClick={() => {
                            this.setState({ showEvacuateModal: true });
                          }}>hello</Button> */}

                       
                        
                        &nbsp;
                      </Row>
                    </Col>
                   
                    <Col md={1}>
                      <FontAwesomeIcon
                        className="action-icon"
                        icon={faPrint}
                        onClick={() => {
                         const data = this.state.employeesData.map((emp, index) => {
                          return {
                            id: index+1, 
                          name: emp.name, 
                          date: emp.date,
                          checkInTime: emp.checkInTime,
                          checkOutTime: emp.checkOutTime
                          }
                         })
                          PrintJs({printable: data, type: 'json', properties: ['id', 'name', 'date','checkInTime','checkOutTime']})
                        }}
                      />{" "}
                      &nbsp; &nbsp;
                      {/* <FontAwesomeIcon
                        className="action-icon"
                        icon={faFileCsv}
                      />{" "} */}
                      {/* </OverlayTrigger> */}
                    </Col>
                  </Row>
                }
                content={
                  <Tab.Container
                    id="tabs-with-dropdown"
                    defaultActiveKey="timeline"
                    activeKey={
                      this.state.activeTab ? this.state.activeTab : "timeline"
                    }
                    onSelect={e => {
                      this.setState({
                        activeTab: e
                      });
                    }}
                  >
                    <Row className="clearfix">
                      <Col sm={12}>
                        <Nav bsStyle="tabs">
                          <NavItem eventKey="timeline">Timeline</NavItem>
                          <NavItem eventKey="list">List</NavItem>
                        </Nav>
                      </Col>
                      <Col sm={12}>
                        <Tab.Content animation>
                          <Tab.Pane eventKey="timeline">
                            <ReactTable
                              data={
                                this.state.employeesData
                                  ? this.state.employeesData
                                  : []
                              }
                              filterable
                              columns={EMPLOYEE_TIMELINE_TABLE_COLUMNS}
                              defaultPageSize={10}
                              showPaginationBottom={true}
                              className="-striped -highlight"
                            />
                          </Tab.Pane>
                          <Tab.Pane eventKey="list">
                            <ReactTable
                              data={
                                this.state.listData ? this.state.listData : []
                              }
                              filterable
                              columns={EMPLOYEES_TABLE_COLUMNS}
                              defaultPageSize={10}
                              showPaginationBottom={true}
                              className="-striped -highlight"
                            />
                          </Tab.Pane>
                        </Tab.Content>
                      </Col>
                    </Row>
                  </Tab.Container>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
