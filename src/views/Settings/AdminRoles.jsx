import React, { Component } from "react";
import { Grid, Row, Col, Table } from "react-bootstrap";

import Card from "../../components/Card/Card.jsx";

import Button from "../../components/CustomButton/CustomButton.jsx";

class RegularForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: "1",
      radioVariant: "1",
      data: [
        {
          fname: "Basil",
          lname: "Arackal",
          email: "basilarackal@lmntrx.com",
          location: "NY",
          arole: "superadmin"
        },
        {
          fname: "Livin",
          lname: "Mathew",
          email: "livin@lmntrx.com",
          location: "NY",
          arole: "superadmin"
        },
        {
          fname: "Aswin",
          lname: "Mathew",
          email: "livin@lmntrx.com",
          location: "NY",
          arole: `<Button>Promote to Admin</Button>`
        }
      ]
    };
  }
  handleRadio = event => {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  };
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title={
                  <Row>
                    <Col md={10}>
                      <h3>Company Administrators</h3>
                    </Col>
                    {/* <Col md={2}>
                      <Button bsStyle="success" bsSize="sm" fill>
                        Add Admin
                      </Button>
                    </Col> */}
                  </Row>
                }
                content={
                  <Table striped responsive>
                    <thead>
                      <tr>
                        <th className="text-center">#</th>
                        <th className="text-center">Firstname</th>
                        <th className="text-center">Lastname</th>
                        <th className="text-center">Email</th>
                        <th className="text-center">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td className="text-center">1</td>
                        <td className="text-center">Andrew </td>
                        <td className="text-center">Mike</td>
                        <td className="text-center">contact@lmntrx.com</td>
                        <td className="text-center">
                          <Button bsStyle="danger">Remove Admin</Button>
                        </td>
                      </tr>
                      <tr>
                        <td className="text-center">2</td>
                        <td className="text-center">John </td>
                        <td className="text-center">Doe</td>
                        <td className="text-center">contact@lmntrx.com</td>
                        <td className="text-center">
                          <Button bsStyle="success">Promote</Button>
                        </td>
                      </tr>
                      <tr>
                        <td className="text-center">3</td>
                        <td className="text-center">Alex</td>
                        <td className="text-center">Mike</td>
                        <td className="text-center">contact@lmntrx.com</td>
                        <td className="text-center">
                          <Button bsStyle="success">Promote</Button>
                        </td>
                      </tr>
                      <tr>
                        <td className="text-center">4</td>
                        <td className="text-center">Mike </td>
                        <td className="text-center">Monday</td>
                        <td className="text-center">contact@lmntrx.com</td>
                        <td className="text-center">
                          <Button bsStyle="success">Promote</Button>
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default RegularForms;
