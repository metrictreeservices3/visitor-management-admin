import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import Switch from "react-bootstrap-switch";

import Card from "../../components/Card/Card.jsx";

class RegularForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: "1",
      radioVariant: "1"
    };
  }
  handleRadio = event => {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  };
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Photo and ID"
                content={
                  <div>
                    <Row>
                      <Col md={6}>
                        {" "}
                        Visitor Photo Capture{" "}
                        <Switch onText="✔" offText="✘" defaultValue={true} />
                      </Col>
                      <Col md={6}>
                        Employee Photo Capture{" "}
                        <Switch onText="✔" offText="✘" defaultValue={false} />
                      </Col>
                    </Row>
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default RegularForms;
