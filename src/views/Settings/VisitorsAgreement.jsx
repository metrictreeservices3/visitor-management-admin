import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import Switch from "react-bootstrap-switch";
import Card from "../../components/Card/Card.jsx";

import Button from "../../components/CustomButton/CustomButton.jsx";
import RichTextEditor from "react-rte";
import { handleError, getSettings } from "../../utils/commons";
import axios from "axios";
import { API_URL, COMMON_HEADER } from "../../utils/constants";
import NotificationManager from "../../utils/Notifications";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faSave, faHistory } from "@fortawesome/free-solid-svg-icons";

class RegularForms extends Component {
  access_token = localStorage.getItem("access_token");
  default_content = `
  <h3>Visitors are welcome to visit during hours of operations. For your safety &amp; security we have the following guidelines:</h3>
  <ol>
    <li>Agree to follow the divisions rules before entry is permitted into the building.</li>
    <li>All visitors must sign in and out through the <strong>main entrance lobby</strong>.</li>
    <li>All visitors are required to read and acknowledge the Non-Disclosure and Waiver Agreement.</li>
    <li>Smoking/tobacco use is <strong>prohibited</strong> in our facility. Please use designated outside areas.</li>
    <li>Firearms/weapons are <strong>prohibited</strong> in our facility.</li>
    <li>In the event of an emergency. Follow signage to the designated muster point.</li>
  </ol>
  <h3>Visitors Non-Disclosure and Waiver Agreement</h3>
  <p><em>During my visit to your facility, I will learn and/or have disclosed to me proprietary or confidential information (including, without limitations, information relating to technology, trade secrets, processes, materials, equipment, drawings, specifications, prototypes and products) and may receive samples of products which not generally known to the public (hereinafter collectively called “Confidential Information”).</em></p>
  <h3>In consideration of your permission to visit your facility &amp; for the courtesies extended to me during my visit:</h3>
  <ol>
    <li>I agree that I will not, without your written permission or that of your authorized representative, either;&nbsp;
      <ol>
        <li>Disclose or otherwise make available to others any Confidential Information disclosed to me during this and any subsequent visit which
          <ol>
            <li>was not known to me or my organization prior to disclosure by you, or&nbsp;</li>
            <li>is not now or subsequently becomes a part of the public domain as a result of publication or otherwise; or&nbsp;</li>
          </ol>
        </li>
        <li>Use or assist others in using or further developing in any manner any confidential information.</li>
        <li>Use cameras or video technology to disclose confidential information.</li>
      </ol>
    </li>
    <li>I also agree to conform to any applicable safety requirements, which are brought to my attention by any employee or by signs posted in the areas that I visit while on the premises, and to observe other reasonable safety precautions.</li>
  </ol>
  <p><em><strong>Thank you.</strong></em></p>
  `;

  state = {
    raw_content: this.default_content,
    initialValue: RichTextEditor.createValueFromString(
      this.default_content,
      "html"
    ),
    value: RichTextEditor.createValueFromString(this.default_content, "html"),
    enableVisitorAgreement: true,
    editmode: false
  };
  onChange = value => {
    this.setState({ value });
  };
  componentDidMount() {
    getSettings(this, ["visitorAgreement"]);
  }
  updateVisitorAgreement() {
    let agreement = this.state.value.toString("html");
    let loader = NotificationManager.ongoing(
      this,
      "Updating Visitor Agreement.",
      "spinner"
    );
    axios
      .put(
        `${API_URL}/settings/agreement`,
        {
          heading: "",
          subHeading: "",
          text: agreement
        },
        COMMON_HEADER(this.access_token)
      )
      .then(response => {
        NotificationManager.removeNotification(this, loader);
        NotificationManager.success(this, "Updated Successfully");
      })
      .catch(error => {
        NotificationManager.removeNotification(this, loader);
        NotificationManager.error(this, "Failed to update.");
        NotificationManager.error(this, handleError(error));
      });
  }
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Agreement Settings"
                content={
                  <Row>
                    <Col md={4}>
                      <Col md={8}>Enable Visitor Agreement</Col>
                      <Col md={2}>
                        <Switch
                          onText="✔"
                          offText="✘"
                          defaultValue={this.state.enableVisitorAgreement}
                          onChange={e => {
                            this.setState({
                              enableVisitorAgreement: !this.state
                                .enableVisitorAgreement
                            });
                          }}
                        />
                      </Col>
                    </Col>

                    {/* <Col md={3}>
                      <Button bsStyle="primary" fill>
                        Upload Logo
                      </Button>
                    </Col> */}
                  </Row>
                }
              />
            </Col>
          </Row>

          <Row hidden={!this.state.enableVisitorAgreement}>
            <Col md={12}>
              <Card
                title={
                  <Row>
                    <Col md={9}>Visitors Agreement</Col>
                    {/* <Col md={1}>
                      <Button bsStyle="danger" bsSize="sm" fill>
                        Edit
                      </Button>
                    </Col> */}
                    <Col md={1}>
                      <Button
                        bsStyle="success"
                        fill
                        onClick={() => {
                          if (this.state.editmode)
                            this.updateVisitorAgreement();
                          this.setState({
                            editmode: !this.state.editmode,
                            initialValue: this.state.value
                          });
                        }}
                      >
                        <FontAwesomeIcon
                          icon={this.state.editmode ? faSave : faEdit}
                        />{" "}
                        {this.state.editmode ? `Save` : `Edit`}
                      </Button>
                    </Col>
                    <Col md={1}>
                      <Button
                        bsStyle="info"
                        fill
                        onClick={() => {
                          this.setState({
                            value: this.state.initialValue
                          });
                        }}
                      >
                        <FontAwesomeIcon icon={faHistory} /> Reset
                      </Button>
                    </Col>
                  </Row>
                }
                content={
                  <Row>
                    <Col md={12}>
                      <RichTextEditor
                        readOnly={!this.state.editmode}
                        value={this.state.value}
                        onChange={this.onChange}
                      />
                    </Col>
                  </Row>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default RegularForms;
