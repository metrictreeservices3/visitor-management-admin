import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import Switch from "react-bootstrap-switch";

import Card from "../../components/Card/Card.jsx";

class RegularForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: "1",
      radioVariant: "1"
    };
  }
  handleRadio = event => {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  };
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Row>
                <Col md={12}>
                  <Card
                    title="Notifications Settings"
                    content={
                      <div>
                        <Row>
                          <Col md={1}>
                            <Switch
                              onText="✔"
                              offText="✘"
                              defaultValue={false}
                            />
                          </Col>

                          <Col md={4}>
                            <strong>Use Email Notifications</strong>
                          </Col>
                        </Row>
                        <br />
                        <Row>
                          <Col md={1}>
                            <Switch
                              onText="✔"
                              offText="✘"
                              defaultValue={false}
                            />
                          </Col>

                          <Col md={4}>
                            <strong>Use SMS Notifications</strong>
                          </Col>
                        </Row>
                      </div>
                    }
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default RegularForms;
