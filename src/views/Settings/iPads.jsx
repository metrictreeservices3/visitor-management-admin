import React, { Component } from "react";
import { Grid, Row, Col, FormControl } from "react-bootstrap";
import Select from "react-select";
import { selectOptions } from "../../variables/Variables.jsx";

import Card from "../../components/Card/Card.jsx";

import Button from "../../components/CustomButton/CustomButton.jsx";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTrash,
  faSave,
  faTablet,
  faLocationArrow
} from "@fortawesome/free-solid-svg-icons";
import "../../assets/css/ipad-settings.css"

import { TIMEZONES, API_URL, COMMON_HEADER } from "../../utils/constants";

import axios from "axios";

import NotificationManager from "../../utils/Notifications";
import { handleError } from "../../utils/commons";

class RegularForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newiPadName: "",
      newLocationName: "",
      iPads: [],
      locations: [],
      timezones: TIMEZONES.map(tz => {
        return {
          value: tz,
          label: tz
        };
      })
    };
  }
  componentDidMount() {
    this.getDevices();
    this.getLocations();
  }
  getDevices() {
    let access_token = localStorage.getItem("access_token");
    axios
      .get(`${API_URL}/device`, COMMON_HEADER(access_token))
      .then(response => {
        this.setState({
          iPads: response.data.data
        });
      })
      .catch(error => {
        NotificationManager.error(this, handleError(error));
      });
  }
  getLocations() {
    let access_token = localStorage.getItem("access_token");
    axios
      .get(`${API_URL}/location`, COMMON_HEADER(access_token))
      .then(response => {
        this.setState({
          locations: response.data.data
        });
      })
      .catch(error => {
        NotificationManager.error(this, handleError(error));
      });
  }
  deleteDevice(id) {
    let access_token = localStorage.getItem("access_token");
    let loader = NotificationManager.ongoing(
      this,
      "Deleting device.",
      "spinner"
    );
    axios
      .delete(`${API_URL}/device/remove/${id}`, COMMON_HEADER(access_token))
      .then(response => {
        NotificationManager.removeNotification(this, loader);
        NotificationManager.success(this, "Deleted.");
        this.getDevices();
        this.getLocations();
      })
      .catch(error => {
        NotificationManager.removeNotification(this, loader);
        NotificationManager.error(this, handleError(error));
      });
  }
  addLocation() {
    if (!this.state.newLocationName || !this.state.newLocationTimeZone) {
      NotificationManager.warn(
        this,
        "Please input a name and select a timezone."
      );
      return;
    }
    let access_token = localStorage.getItem("access_token");
    let loader = NotificationManager.ongoing(
      this,
      "Adding location.",
      "spinner"
    );
    axios
      .post(
        `${API_URL}/location/add/`,
        {
          name: this.state.newLocationName,
          timezone: this.state.newLocationTimeZone.value
        },
        COMMON_HEADER(access_token)
      )
      .then(response => {
        NotificationManager.removeNotification(this, loader);
        NotificationManager.success(this, "Added.");
        this.setState({
          newLocationName: "",
          newLocationTimeZone: null
        });
        this.getLocations();
      })
      .catch(error => {
        NotificationManager.removeNotification(this, loader);
        NotificationManager.error(this, handleError(error));
      });
  }
  addDevice() {
    if (!this.state.newiPadName || !this.state.newiPadLocation) {
      NotificationManager.warn(
        this,
        "Please input a name and select a location."
      );
      return;
    }
    let access_token = localStorage.getItem("access_token");
    let loader = NotificationManager.ongoing(this, "Adding Device.", "spinner");
    axios
      .post(
        `${API_URL}/device/add/`,
        {
          name: this.state.newiPadName,
          location: this.state.newiPadLocation.value
        },
        COMMON_HEADER(access_token)
      )
      .then(response => {
        NotificationManager.removeNotification(this, loader);
        NotificationManager.success(this, "Added.");
        this.setState({
          newiPadName: "",
          newiPadLocation: null
        });
        this.getDevices();
        this.getLocations();
      })
      .catch(error => {
        NotificationManager.removeNotification(this, loader);
        NotificationManager.error(this, handleError(error));
      });
  }
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={6}>
              <Card
                title={"iPads"}
                content={
                  <div>
                    {this.state.iPads.map((iPad, index, iPads) => {
                      return (
                        <Card
                          key={iPad.id}
                          content={
                            <div>
                              <Row>
                                <Col md={12}>
                                  <h2>
                                    <strong>DEVICE ID: </strong>{" "}
                                    {iPad.deviceIdentifier}
                                  </h2>
                                </Col>
                              </Row>
                              <Row>
                                <Col md={12} >
                                  <FormControl
                                    placeholder="Reception"
                                    type="text"
                                    value={iPad.name}
                                    onChange={e => {
                                      let nArray = iPads;
                                      nArray[index] = {
                                        ...iPad,
                                        name: e.target.value
                                      };
                                      this.setState({
                                        iPads: nArray
                                      });
                                    }}
                                  />
                                </Col>
                              </Row>
                              <br />
                              <Row>
                                <Col md={12} >
                                  <Select
                                    placeholder="Head Office"
                                    name="singleSelect"
                                    value={
                                      iPad.locationSelect
                                        ? iPad.locationSelect
                                        : {
                                            value: iPad.location,
                                            label: iPad.location
                                              ? iPad.locationDetails.name
                                              : null
                                          }
                                    }
                                    options={this.state.locations.map(
                                      location => {
                                        return {
                                          value: location.id,
                                          label: location.name
                                        };
                                      }
                                    )}
                                    clearable={false}
                                    onChange={value => {
                                      if (value) {
                                        let nArray = iPads;
                                        nArray[index] = {
                                          ...iPad,
                                          location: value.value,
                                          locationSelect: value
                                        };
                                        this.setState({
                                          iPads: nArray
                                        });
                                      }
                                    }}
                                  />
                                </Col>
                              </Row>
                              <br />
                              <Row>
                                <Col md={6} align="center">
                                  <Button
                                    bsStyle="danger"
                                    fill
                                    wd
                                    onClick={() => {
                                      this.deleteDevice(iPad.id);
                                    }}
                                  >
                                    <FontAwesomeIcon icon={faTrash} /> Delete
                                  </Button>
                                </Col>
                                <Col md={6} align="center">
                                  <Button bsStyle="success" fill wd>
                                    <FontAwesomeIcon icon={faSave} /> Update
                                  </Button>
                                </Col>
                              </Row>
                            </div>
                          }
                        />
                      );
                    })}

                    <Row
                      style={{
                        padding: "2em"
                      }}
                    >
                      <Col md={10}>
                        <FormControl
                          placeholder="New iPad Name"
                          type="text"
                          value={this.state.newiPadName}
                          onChange={e => {
                            this.setState({
                              newiPadName: e.target.value
                            });
                          }}
                        />
                      </Col>
                      <Col md={10}  className={'inpt-margin'}>
                        <Select
                          placeholder="Head Office"
                          name="singleSelect"
                          value={this.state.newiPadLocation}
                          options={this.state.locations.map(location => {
                            return {
                              value: location.id,
                              label: location.name
                            };
                          })}
                          defaultValue={
                            this.state.locations.map(location => {
                              return {
                                value: location.id,
                                label: location.name
                              };
                            })[0]
                          }
                          onChange={value =>
                            this.setState({ newiPadLocation: value })
                          }
                        />
                      </Col>
                      <Col md={10}  className={'inpt-margin'}>
                        <Button
                          bsStyle="primary"
                          fill
                          onClick={() => {
                            this.addDevice();
                          }}
                        >
                          <FontAwesomeIcon icon={faTablet} /> Add New iPad
                        </Button>
                      </Col>
                    </Row>
                  </div>
                }
              />
            </Col>
            <Col md={6}>
              <Card
                title="Locations"
                content={
                  <div>
                    {this.state.locations.map((location, index, locations) => {
                      return (
                        <Row key={index}>
                          <Col md={10}>
                            <FormControl
                              placeholder="Head Office"
                              type="text"
                              value={location.name}
                              onChange={e => {
                                let nArray = locations;
                                nArray[index] = {
                                  ...location,
                                  name: e.target.value
                                };
                                this.setState({
                                  locations: nArray
                                });
                              }}
                            />
                          </Col>
                          <Col md={10} className={'inpt-margin'}>
                            <Select
                              placeholder="(GMT-11:00) Pacific, Midway"
                              name="singleSelect"
                              value={
                                location.timezoneSelect
                                  ? location.timezoneSelect
                                  : {
                                      value: location.timezone,
                                      label: location.timezone
                                    }
                              }
                              clearable={false}
                              options={this.state.timezones}
                              defaultValue={selectOptions[1]}
                              onChange={value => {
                                if (value) {
                                  let nArray = locations;
                                  nArray[index] = {
                                    ...location,
                                    timezone: value.value,
                                    timezoneSelect: value
                                  };
                                  this.setState({
                                    locations: nArray
                                  });
                                }
                              }}
                            />
                          </Col>
                          <Col md={3}>
                            <strong>
                              {location.ipads}{" "}
                              {location.ipads === 1 ? "iPad" : "iPads"}
                            </strong>
                          </Col>
                        </Row>
                      );
                    })}
                    <br />
                    <Row>
                      <Col md={10}>
                        <FormControl
                          placeholder="Branch Office"
                          type="text"
                          value={this.state.newLocationName}
                          onChange={e => {
                            this.setState({
                              newLocationName: e.target.value
                            });
                          }}
                        />
                      </Col>
                      <Col md={10}  className={'inpt-margin'}>
                        <Select
                          placeholder="(GMT-11:00) Pacific, Midway"
                          name="singleSelect"
                          value={this.state.newLocationTimeZone}
                          options={this.state.timezones}
                          defaultValue={selectOptions[1]}
                          onChange={value =>
                            this.setState({ newLocationTimeZone: value })
                          }
                        />
                      </Col>
                      <Col md={10}  className={'inpt-margin'}>
                        <Button
                          bsStyle="primary"
                          fill
                          onClick={() => {
                            this.addLocation();
                          }}
                        >
                          <FontAwesomeIcon icon={faLocationArrow} /> Add
                          Location
                        </Button>
                      </Col>
                    </Row>
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default RegularForms;
