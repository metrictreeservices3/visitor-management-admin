import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";

import Card from "../../components/Card/Card.jsx";

class RegularForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: "1",
      radioVariant: "1"
    };
  }
  handleRadio = event => {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  };
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card title="Visitors Fields" />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default RegularForms;
