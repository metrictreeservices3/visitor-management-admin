import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import Switch from "react-bootstrap-switch";

import Card from "../../components/Card/Card.jsx";

class RegularForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio: "1",
      radioVariant: "1"
    };
  }
  handleRadio = event => {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  };
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Visitors Settings"
                content={
                  <div>
                    <Row>
                      <Col md={1}>
                        <Switch onText="✔" offText="✘" defaultValue={false} />
                      </Col>

                      <Col md={11}>
                        <strong>Employee (host) selection</strong>
                        <li className="list-unstyled">
                          <ul>
                            <li>
                              If toggled ON, visitors will be required to select
                              an employee (host) to visit.
                            </li>
                            <li>
                              If toggled OFF, visitors will not be required to
                              select an employee (host) to visit.
                            </li>
                          </ul>
                        </li>
                      </Col>
                    </Row>
                    <br />
                    <Row>
                      <Col md={1}>
                        <Switch onText="✔" offText="✘" defaultValue={false} />
                      </Col>

                      <Col md={11}>
                        <strong>Visitor auto sign out</strong>
                        <li className="list-unstyled">
                          <ul>
                            <li>
                              If toggled ON, visitors who forget to sign out
                              will be auto-signed out at midnight.
                            </li>
                            <li>
                              If toggled OFF, visitors will stay signed in.
                            </li>
                          </ul>
                        </li>
                      </Col>
                    </Row>
                  </div>
                }
              />
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <Card
                title="Privacy Settings"
                content={
                  <div>
                    <Row>
                      <Col md={1}>
                        <Switch onText="✔" offText="✘" defaultValue={false} />
                      </Col>

                      <Col md={11}>
                        <strong>Employee (host) selection - List view</strong>{" "}
                        <li className="list-unstyled">
                          <ul>
                            <li>
                              If toggled ON, visitors are presented with a list
                              of employees (hosts) to choose from.
                            </li>
                            <li>
                              If toggled OFF, visitors are required to search
                              for their host by entering the hosts full name.
                              Recommended for GDPR/privacy purposes*
                            </li>
                          </ul>
                        </li>
                      </Col>
                    </Row>
                    <br />
                    <Row>
                      <Col md={1}>
                        <Switch onText="✔" offText="✘" defaultValue={false} />
                      </Col>

                      <Col md={11}>
                        <strong>Saved visitor (‘Remember me’)</strong>
                        <li className="list-unstyled">
                          <ul>
                            <li>
                              If toggled ON, a ‘Remember me’ toggle will appear
                              on the visitor sign in. If a visitor chooses to
                              use this, their details will be remembered for a
                              faster sign in next time.
                            </li>
                            <li>
                              If toggled OFF, visitors will not be remembered.
                              Recommended for GDPR/privacy purposes*
                            </li>
                          </ul>
                        </li>
                      </Col>
                    </Row>
                    <br />
                    <Row>
                      <Col md={1}>
                        <Switch onText="✔" offText="✘" defaultValue={false} />
                      </Col>

                      <Col md={11}>
                        <strong>Auto-suggest</strong>
                        <li className="list-unstyled">
                          <ul>
                            <li>
                              If toggled ON, SwipedOn will auto-suggest visitor
                              names to choose from (allowing quicker sign in /
                              out) once the first 3 letters are entered.
                            </li>
                            <li>
                              If toggled OFF, visitors will be required to enter
                              their full name and company name. Recommended for
                              GDPR/privacy purposes*
                            </li>
                          </ul>
                        </li>
                      </Col>
                    </Row>
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default RegularForms;
