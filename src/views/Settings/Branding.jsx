import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormControl,
  OverlayTrigger,
  Tooltip,
  Table
} from "react-bootstrap";

import Card from "../../components/Card/Card.jsx";

import Button from "../../components/CustomButton/CustomButton.jsx";
import { getSettings, handleError, parseColor } from "../../utils/commons";
import axios from "axios";
import { API_URL, COMMON_HEADER } from "../../utils/constants";
import NotificationManager from "../../utils/Notifications";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import ColorPicker from "../../components/ColorPicker/ColorPicker.jsx";

class RegularForms extends Component {
  access_token = localStorage.getItem("access_token");

  state = {
    current_backgroud: 0,
    backgroundImages: [],
    visitor_in_button_text: "Visitor In",
    visitor_out_button_text: "Visitor Out",
    accentColor: "#498f9c",
    homeScreenButtonColor: "#ffffff",
    buttonTextColor: "#000000",
    employeeSwipeButtonColor: "#004001"
  };

  constructor(props) {
    super(props);
    this.props = props;
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      let c =
        this.state.current_backgroud < this.state.backgroundImages.length - 1
          ? this.state.current_backgroud + 1
          : 0;
      this.setState({
        current_backgroud: c
      });
    }, 5000);
    getSettings(this, ["screenDesign", "screenSaver"]);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  deleteScreenSaver(id) {
    let loader = NotificationManager.ongoing(this, "Deleting", "spinner");
    axios
      .delete(
        `${API_URL}/settings/branding/screensaver/${id}`,
        COMMON_HEADER(this.access_token)
      )
      .then(response => {
        NotificationManager.removeNotification(this, loader);
        if (
          response.data &&
          response.data.data &&
          response.data.data.screenSaver
        )
          this.setState({
            backgroundImages: response.data.data.screenSaver.map(item => {
              return {
                id: item["id"],
                url: item["url"].startsWith("http")
                  ? item["url"]
                  : `${API_URL}/${item["url"]}`
              };
            })
          });
        else NotificationManager.error(this, "Failed to delete image.");
      })
      .catch(error => {
        NotificationManager.removeNotification(this, loader);
        NotificationManager.error(this, handleError(error));
        console.error(JSON.stringify(error));
        NotificationManager.error(this, "Failed to delete image.");
      });
  }

  uploadFile(e) {
    let files = e.target.files;
    if (!files || files.length === 0) return;
    let loader = NotificationManager.ongoing(
      this,
      "Uploading Image",
      "spinner"
    );
    const formData = new FormData();
    for (var i = 0; i < files.length; i++) {
      formData.append("photos", files[i]);
    }
    axios
      .post(`${API_URL}/settings/branding/screensaver`, formData, {
        headers: {
          "content-type": "multipart/form-data",
          "x-access-token": this.access_token
        }
      })
      .then(response => {
        NotificationManager.removeNotification(this, loader);
        if (
          response.data &&
          response.data.data &&
          response.data.data.screenSaver
        )
          this.setState({
            backgroundImages: response.data.data.screenSaver.map(item => {
              return {
                id: item["id"],
                url: item["url"].startsWith("http")
                  ? item["url"]
                  : `${API_URL}/${item["url"]}`
              };
            })
          });
        else NotificationManager.error(this, "Failed to upload image.");
      })
      .catch(error => {
        NotificationManager.removeNotification(this, loader);
        NotificationManager.error(this, "Failed to upload image.");
        NotificationManager.error(this, handleError(error));
      });
  }

  updateDesign(change) {
    let loader = NotificationManager.ongoing(this, "Updating Text", "spinner");
    axios
      .put(
        `${API_URL}/settings/branding/screen_design`,
        change,
        COMMON_HEADER(this.access_token)
      )
      .then(reponse => {
        NotificationManager.removeNotification(this, loader);
      })
      .catch(error => {
        NotificationManager.removeNotification(this, loader);
        NotificationManager.error(this, "Update Failed.");
        NotificationManager.error(this, handleError(error));
      });
  }

  updateVisitorButtonText() {
    this.updateDesign({
      visitorButtonText: {
        in: this.state.visitor_in_button_text,
        out: this.state.visitor_out_button_text
      }
    });
  }
  updateColors() {
    this.updateDesign({
      homeScreenButtonColor: this.state.homeScreenButtonColor,
      accentColor: this.state.accentColor,
      buttonTextColor: this.state.buttonTextColor,
      employeeSwipeButtonColor: this.state.employeeSwipeButtonColor
    });
  }

  removePost = <Tooltip id="remove">Remove Photo</Tooltip>;

  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Preview"
                content={
                  <div style={{ display: "flex", justifyContent: "center" }}>
                    <Row>
                      <Col md={12}>
                        <div className="img-container preview-image-container">
                          <img
                            alt="..."
                            src={
                              this.state.backgroundImages[
                                this.state.current_backgroud
                              ]
                                ? this.state.backgroundImages[
                                    this.state.current_backgroud
                                  ].url
                                : ""
                            }
                            width="640px"
                            height="450px"
                          />

                          <Button
                            bsStyle="customVisitorIn"
                            fill
                            round
                            wd
                            style={{
                              color: this.state.buttonTextColor,
                              backgroundColor: this.state.homeScreenButtonColor
                            }}
                          >
                            {this.state.visitor_in_button_text}
                          </Button>
                          <Button
                            bsStyle="customVisitorOut"
                            fill
                            round
                            wd
                            style={{
                              color: this.state.buttonTextColor,
                              backgroundColor: this.state.homeScreenButtonColor
                            }}
                          >
                            {this.state.visitor_out_button_text}
                          </Button>
                          <h1 className="preview-image-welcome">Welcome</h1>
                        </div>
                      </Col>
                    </Row>
                  </div>
                }
              />

              <Row>
                <Col md={6}>
                  <Card
                    title="Colors"
                    content={
                      <div>
                        <Card
                          title="Button Background"
                          content={
                            <ColorPicker
                              color={parseColor(
                                this.state.homeScreenButtonColor
                              )}
                              handler={color => {
                                this.setState({
                                  homeScreenButtonColor: `rgba(${
                                    color.rgb.r
                                  }, ${color.rgb.g}, ${color.rgb.b}, ${
                                    color.rgb.a
                                  })`
                                });
                              }}
                            />
                          }
                        />

                        <Card
                          title="Button Text"
                          content={
                            <ColorPicker
                              color={parseColor(this.state.buttonTextColor)}
                              handler={color => {
                                this.setState({
                                  buttonTextColor: `rgba(${color.rgb.r}, ${
                                    color.rgb.g
                                  }, ${color.rgb.b}, ${color.rgb.a})`
                                });
                              }}
                            />
                          }
                        />
                        <Button
                          bsStyle="info"
                          fill
                          onClick={() => {
                            this.updateColors();
                          }}
                        >
                          Save
                        </Button>
                      </div>
                    }
                  />
                  <Card
                    title="Change Visitor Button Text"
                    content={
                      <div>
                        <FormControl
                          placeholder="Visitors In"
                          type="text"
                          value={this.state.visitor_in_button_text}
                          onChange={e => {
                            this.setState({
                              visitor_in_button_text: e.target.value
                            });
                          }}
                        />
                        <br />
                        <FormControl
                          placeholder="Visitors Out"
                          type="text"
                          value={this.state.visitor_out_button_text}
                          onChange={e => {
                            this.setState({
                              visitor_out_button_text: e.target.value
                            });
                          }}
                        />
                        <br />
                        <Button
                          bsStyle="info"
                          fill
                          onClick={() => {
                            this.updateVisitorButtonText();
                          }}
                        >
                          Save
                        </Button>
                      </div>
                    }
                  />

                  {/* <Card
                    title="Change Employee Swipe Button Text"
                    content={
                      <div>
                        <FormControl placeholder="Employees" type="text" />
                        <br />
                        <Button bsStyle="info" fill>
                          Save
                        </Button>
                      </div>
                    }
                  /> */}
                </Col>

                <Col md={6}>
                  <Card
                    title="iPad Screen Saver"
                    content={
                      <div>
                        <Col md={12}>
                          <Card
                            textCenter
                            title="Images"
                            tableFullWidth
                            content={
                              <Table responsive className="table-bigboy">
                                <tbody>
                                  {this.state.backgroundImages.map(
                                    (image, index) => {
                                      return (
                                        <tr key={index}>
                                          <td className="img-container">
                                            <div>
                                              <img alt="..." src={image.url} />
                                            </div>
                                          </td>
                                          <td className="td-action-button">
                                            <OverlayTrigger
                                              placement="left"
                                              overlay={this.removePost}
                                            >
                                              <Button
                                                simple
                                                icon
                                                bsStyle="danger"
                                                onClick={() =>
                                                  this.deleteScreenSaver(
                                                    image.id
                                                  )
                                                }
                                              >
                                                <FontAwesomeIcon
                                                  icon={faTrash}
                                                />
                                              </Button>
                                            </OverlayTrigger>
                                          </td>
                                        </tr>
                                      );
                                    }
                                  )}
                                  {/* <tr>
                                    <td>
                                      <div className="img-container">
                                        <img
                                          alt="..."
                                          src={
                                            "https://picsum.photos/200/300/?random"
                                          }
                                        />
                                      </div>
                                    </td>

                                    {this.actionsPost}
                                  </tr>
                                  <tr>
                                    <td>
                                      <div className="img-container">
                                        <img
                                          alt="..."
                                          src={
                                            "https://picsum.photos/200/300/?random"
                                          }
                                        />
                                      </div>
                                    </td>

                                    {this.actionsPost}
                                  </tr>
                                  <tr>
                                    <td>
                                      <div className="img-container">
                                        <img
                                          alt="..."
                                          src={
                                            "https://picsum.photos/200/300/?random"
                                          }
                                        />
                                      </div>
                                    </td>

                                    {this.actionsPost}
                                  </tr>
                                 */}
                                </tbody>
                              </Table>
                            }
                          />
                        </Col>
                        Add new Screen Saver image{" "}
                        <Button
                          bsStyle="info"
                          fill
                          onClick={() => {
                            this.upload.click();
                          }}
                        >
                          <input
                            id="myInput"
                            type="file"
                            accept="image/*"
                            ref={ref => (this.upload = ref)}
                            style={{ display: "none" }}
                            onChange={e => this.uploadFile(e)}
                            multiple
                          />
                          Upload New Image
                        </Button>
                      </div>
                    }
                  />
                </Col>
              </Row>

              <Row>
                <Col md={6} />
                <Col md={6} />
              </Row>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default RegularForms;
