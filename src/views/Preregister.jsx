import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";
import Datetime from "react-datetime";
import Creatable from "react-select/lib/Creatable";
import Select from "react-select";
import ReactTable from "react-table";

import Card from "../components/Card/Card.jsx";

import Button from "../components/CustomButton/CustomButton.jsx";
import { API_URL, DATE_OPTIONS, COMMON_HEADER } from "../utils/constants";
import axios from "axios";
import {
  handleError,
  getVisitorsList,
  getEmployeesList,
  visitorDataIsNotValid,
  getAllEmployees
} from "../utils/commons";
import NotificationManager from "../utils/Notifications";

class RegularForms extends Component {
  state = {
    from: new Date().toISOString(),
    to: new Date().toISOString()
  };
  access_token = localStorage.getItem("access_token");
  componentWillMount() {
    this.getPregisteredVisitors();
    getVisitorsList(this);
    // getEmployeesList(this);
    getAllEmployees(this)
  }
  getPregisteredVisitors() {
    let loader = NotificationManager.ongoing(
      this,
      "Loading pre-registered visitors.",
      "spinner"
    );
    axios
      .get(`${API_URL}/session/preregister`, COMMON_HEADER(this.access_token))
      .then(response => {
        NotificationManager.removeNotification(this, loader);
        let preregistered_visitors = [];
        response.data.data.forEach(visitor => {
          preregistered_visitors.push({
            visitor: visitor.visitorDetails.name.trim(),
            date: new Date(visitor.from).toLocaleDateString(
              "en-Us",
              DATE_OPTIONS
            ),
            companyName: visitor.visitorDetails.companyName.trim(),
            visiting: visitor.hostDetails.name.trim()
          });
        });
        this.setState({
          preregistered_visitors: preregistered_visitors.reverse()
        });
      })
      .catch(error => {
        NotificationManager.error(this, handleError(error));
      });
  }
  submit() {
    if (visitorDataIsNotValid(this)) return;
    let loader = NotificationManager.ongoing(this, "Adding Visitor", "spinner");
    const access_token = localStorage.getItem("access_token");
    let url = `${API_URL}/session/preregister`;
    let body = {
      email: this.state.email,
      name: this.state.name,
      location: this.state.location,
      from: this.state.from,
      to: this.state.to,
      companyName: this.state.companyName,
      host: this.state.selectedEmployeeId
    };
    axios
      .post(url, body, {
        headers: {
          "x-access-token": access_token
        }
      })
      .then(response => {
        NotificationManager.removeNotification(this, loader);
        this.getPregisteredVisitors();
        this.clearInputs();
      })
      .catch(error => {
        NotificationManager.error(this, handleError(error));
      });
  }
  clearInputs() {
    this.setState({
      email: null,
      name: null,
      location: null,
      from: new Date().toISOString(),
      to: new Date().toISOString(),
      companyName: null,
      host: null,
      singleSelect: null,
      employeeSelect: null
    });
  }
  render() {
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={6}>
              <Card
                title="Add new visitor"
                content={
                  <form>
                    <FormGroup>
                      <ControlLabel>
                        Full name
                        <span className="star">*</span>
                      </ControlLabel>
                      <Creatable
                        placeholder="Tim Cook"
                        name="singleSelect"
                        value={
                          this.state.singleSelect ? this.state.singleSelect : ""
                        }
                        options={
                          this.state.visitorsList ? this.state.visitorsList : []
                        }
                        promptTextCreator={label => `Create Visitor: ${label}`}
                        defaultValue={""}
                        onChange={visitor => {
                          this.setState({
                            nameError: null,
                            singleSelect: visitor,
                            selectedId: visitor ? visitor.id : null,
                            name:
                              visitor && visitor.name
                                ? visitor.name.trim()
                                : visitor.value.trim(),
                            companyName:
                              visitor && visitor.companyName
                                ? visitor.companyName.trim()
                                : null,
                            email:
                              visitor && visitor.email
                                ? visitor.email.trim()
                                : null,
                            location:
                              visitor && visitor.location
                                ? visitor.location.trim()
                                : null
                          });
                        }}
                      />
                      {this.state.nameError}
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Company Name</ControlLabel>
                      <FormControl
                        placeholder="Tesla"
                        type="text"
                        onChange={e => {
                          this.setState({
                            companyName: e.target.value
                          });
                        }}
                        value={
                          this.state.companyName ? this.state.companyName : ""
                        }
                      />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>
                        Email
                        <span className="star">*</span>
                      </ControlLabel>
                      <FormControl
                        placeholder="elon@tesla.com"
                        type="text"
                        onChange={e => {
                          this.setState({
                            emailError: null,
                            email: e.target.value
                          });
                        }}
                        value={this.state.email ? this.state.email : ""}
                      />
                      {this.state.emailError}
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Location</ControlLabel>
                      <FormControl
                        placeholder="New York, USA"
                        type="text"
                        onChange={e => {
                          this.setState({
                            location: e.target.value
                          });
                        }}
                        value={this.state.location ? this.state.location : ""}
                      />
                    </FormGroup>

                    <FormGroup>
                      <ControlLabel>From</ControlLabel>
                      <Datetime
                        timeFormat={true}
                        onChange={e => {
                          this.setState({
                            from: e.toISOString()
                          });
                        }}
                        inputProps={{ placeholder: "Date Picker Here" }}
                        defaultValue={new Date()}
                      />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>To</ControlLabel>
                      <Datetime
                        timeFormat={true}
                        onChange={e => {
                          this.setState({
                            to: e.toISOString()
                          });
                        }}
                        inputProps={{ placeholder: "Date Picker Here" }}
                        defaultValue={new Date()}
                      />
                    </FormGroup>

                    <FormGroup>
                      <ControlLabel>
                        Visiting
                        <span className="star">*</span>
                      </ControlLabel>
                      {console.log(this.state.employeeSelect)}
                      <Select
                        placeholder="Tim Cook"
                        name="employeeSelect"
                        value={
                          this.state.employeeSelect
                            ? this.state.employeeSelect
                            : ""
                          // this.state.employeesList
                          //   ? this.state.employeesList
                          //   : []
                        }
                        options={
                          this.state.employeesList
                            ? this.state.employeesList
                            : []
                        }
                       
                        defaultValue={
                          this.state.employeesList
                            ? this.state.employeesList[0]
                              ? this.state.employeesList[0]
                              : ""
                            : ""
                        }
                        onChange={employee => {
                          this.setState({
                            hostError: null,
                            employeeSelect: employee,
                            selectedEmployeeId: employee ? employee.value : null
                          });
                        }}
                      />
                      {this.state.hostError}
                    </FormGroup>

                    {/* <FormGroup>
                      <Checkbox
                        isChecked
                        number="1"
                        label="Remember Guest Details"
                      />
                    </FormGroup> */}
                    <Button
                      bsStyle="info"
                      fill
                      onClick={() => {
                        this.submit();
                      }}
                    >
                      Submit
                    </Button>
                  </form>
                }
              />
            </Col>
            <Col md={6}>
              <Card
                title="Pre-registered Visitors"
                content={
                  <ReactTable
                    data={this.state.preregistered_visitors}
                    filterable
                    columns={[
                      {
                        Header: "Visitor",
                        accessor: "visitor",
                        filterable: true
                      },
                      {
                        Header: "Date",
                        accessor: "date",
                        filterable: true
                      },
                      {
                        Header: "Company Name",
                        accessor: "companyName",
                        filterable: true
                      },
                      {
                        Header: "Visiting",
                        accessor: "visiting",
                        filterable: true
                      }
                    ]}
                    defaultPageSize={10}
                    showPaginationBottom={false}
                    className="-striped -highlight"
                  />
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default RegularForms;
