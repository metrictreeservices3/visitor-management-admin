import React, { Component } from 'react';
import axios from 'axios';
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from 'react-bootstrap';

import Card from '../../components/Card/Card.jsx';

import Button from '../../components/CustomButton/CustomButton.jsx';

import NotificationManager from "../../utils/Notifications";

import { API_URL } from "../../utils/constants";


class ForgotPasswordPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardHidden: true,
      email: ''
    };
  }
  componentDidMount() {
    setTimeout(
      function() {
        this.setState({ cardHidden: false });
      }.bind(this),
      700
    );
  }
  submit = () => {
    const self = this;
    console.log(API_URL)
    if(this.state.email) {
      axios
      .post(
        `${API_URL}/auth/forgot-password`,
        {
          email: this.state.email
        }
      )
      .then(response => {
        NotificationManager.success(self, `Reset instructions sent to ${self.state.email}`)
      })
      .catch(err => NotificationManager.error(self, err.response.data.error))
    } else {
      NotificationManager.error(this, "Enter Email")
    }
  }
  render() {
    return (
      <Grid>
        <Row>
          <Col md={4} sm={6} mdOffset={4} smOffset={3}>
            <form>
              <Card
                hidden={this.state.cardHidden}
                textCenter
                title="Reset Account Password"
                content={
                  <div>
                    <FormGroup>
                      <ControlLabel>Email address</ControlLabel>
                      <FormControl placeholder="Enter email" type="email" 
                      value={this.state.email}
                      onChange={e => {
                        this.setState({
                          email: e.target.value
                        })
                      }} />
                    </FormGroup>
                  </div>
                }
                legend={
                  <Button bsStyle="info" fill wd onClick={this.submit}>
                    Reset Password
                  </Button>
                }
                ftTextCenter
              />
            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default ForgotPasswordPage;
