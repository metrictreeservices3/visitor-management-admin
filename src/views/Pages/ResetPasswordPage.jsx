import React, { Component } from 'react';
import axios from 'axios';
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from 'react-bootstrap';

import Card from '../../components/Card/Card.jsx';

import Button from '../../components/CustomButton/CustomButton.jsx';

import NotificationManager from "../../utils/Notifications";

import { API_URL } from "../../utils/constants";


class ForgotPasswordPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardHidden: true,
      password: '',
      confirmPassword: '',
      token: ''
    };
  }
  componentDidMount() {
    setTimeout(
      function() {
        this.setState({ cardHidden: false });
      }.bind(this),
      700
    );
    this.setState({
        token: this.props.match.params.token
    })
  }
  submit = () => {
    const self = this;
    console.log(API_URL, 'token : ', self.state.token)
    !self.state.password 
    ? NotificationManager.error(self, 'Enter password')
    : !self.state.confirmPassword
    ? NotificationManager.error(self, 'Confirm password')
    : self.state.password !== self.state.confirmPassword
    ? NotificationManager.error(self, "Passwords don't match")
    : self.state.password.length < 6
    ? NotificationManager.error(self, 'Minimum password length is 6 characters')
    : (function (context){
        axios.post(`${API_URL}/auth/reset-password`, {password: self.state.password}, {headers: {
            "x-access-token": self.state.token
          }})
          .then(response => {
              NotificationManager.success(self,'Password changed');
              setTimeout(() => {
                localStorage.clear();
                localStorage.setItem("id", response.data.data.id);
                localStorage.setItem("email", response.data.data.email);
                localStorage.setItem("name", response.data.data.name);
                localStorage.setItem("access_token", response.data.data.access_token);
                localStorage.setItem("companyName", response.data.data.companyName);
                window.location.assign("#/dashboard");
              }, 1000);
          })
          .catch(error => {
                NotificationManager.error(self, error.response.data.error);
          })
    })(self)
  }
  render() {
    return (
      <Grid>
        <Row>
          <Col md={4} sm={6} mdOffset={4} smOffset={3}>
            <form>
              <Card
                hidden={this.state.cardHidden}
                textCenter
                title="Enter New Password"
                content={
                  <div>
                    <FormGroup>
                      <ControlLabel>Password</ControlLabel>
                      <FormControl placeholder="Enter new password" type="password" 
                      value={this.state.password}
                      onChange={e => {
                        this.setState({
                          password: e.target.value
                        })
                      }} />
                      </FormGroup>
                      <FormGroup>
                      <ControlLabel>Confirm Password</ControlLabel>
                      <FormControl placeholder="Confirm password" type="password" 
                      value={this.state.confirmPassword}
                      onChange={e => {
                        this.setState({
                          confirmPassword: e.target.value
                        })
                      }} />
                    </FormGroup>
                  </div>
                }
                legend={
                  <Button bsStyle="info" fill wd onClick={this.submit}>
                    Reset Password
                  </Button>
                }
                ftTextCenter
              />
            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default ForgotPasswordPage;
