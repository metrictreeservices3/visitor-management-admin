import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";

import Card from "../../components/Card/Card.jsx";

import Button from "../../components/CustomButton/CustomButton.jsx";

import { API_URL } from "../../utils/constants";
import axios from "axios";
import NotificationManager from "../../utils/Notifications";

class LoginPage extends Component {
  state = {
    email: "",
    password: "",
    button_text: "Log in to Dashboard"
  };

  handleEmailChange(e) {
    this.setState({ email: e.target.value });
  }

  handlePasswordChange(e) {
    this.setState({ password: e.target.value });
  }
  handleKeyPress = e => {
    if (e.key === "Enter") {
      this.login(this.state.email, this.state.password);
    }
  };

  async login() {
    this.setState({ button_text: "Logging in..." });
    let response = await axios
      .post(`${API_URL}/auth/login`, {
        email: this.state.email,
        password: this.state.password
      })
      .catch(error => {
        console.error(error);
        if (
          error &&
          error.response &&
          error.response &&
          error.response.data &&
          error.response.data.status === 401
        ) {
          NotificationManager.error(this, error.response.data.error);
          this.setState({ button_text: "Log in to Dashboard" });
        } else {
          NotificationManager.error(this, "Network Error");
        }
      });
    if (response) {
      localStorage.clear();
      localStorage.setItem("id", response.data.data.id);
      localStorage.setItem("email", response.data.data.email);
      localStorage.setItem("name", response.data.data.name);
      localStorage.setItem("access_token", response.data.data.access_token);
      localStorage.setItem("companyName", response.data.data.companyName);
      window.location.assign("#/dashboard");
    }
  }

  render() {
    return (
      <Grid>
        <Row>
          <Col md={4} sm={6} mdOffset={4} smOffset={3}>
            <form>
              <Card
                textCenter
                title="VMS Login"
                content={
                  <div>
                    <FormGroup>
                      <ControlLabel>Email address</ControlLabel>
                      <FormControl
                        placeholder="Enter email"
                        type="email"
                        onChange={e => {
                          this.handleEmailChange(e);
                        }}
                        onKeyPress={this.handleKeyPress}
                      />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Password</ControlLabel>
                      <FormControl
                        placeholder="Password"
                        type="password"
                        onChange={e => {
                          this.handlePasswordChange(e);
                        }}
                        onKeyPress={this.handleKeyPress}
                      />
                    </FormGroup>
                    <a href="#/auth/forgot_password">Forgot password ?</a>
                  </div>
                }
                legend={
                  <Button
                    onClick={() => {
                      this.login();
                    }}
                    bsStyle="info"
                    fill
                    wd
                    style={{ cursor: "pointer" }}
                  >
                    {this.state.button_text}
                  </Button>
                }
                ftTextCenter
              />
        
            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default LoginPage;
