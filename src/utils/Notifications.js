import React from "react";

export default {
  warn(context, text) {
    return context.props.notificationSystem
      ? context.props.notificationSystem.addNotification({
          level: "warning",
          position: "tr",
          dismissible: "both",
          autoDismiss: 5,
          title: <i className={`fa fa-exclamation-circle`} />,
          message: <div>{text}</div>
        })
      : null;
  },
  success(context, text) {
    return context.props.notificationSystem
      ? context.props.notificationSystem.addNotification({
          level: "success",
          position: "tr",
          dismissible: "both",
          autoDismiss: 5,
          title: <i className={`fa fa-check-circle`} />,
          message: <div>{text}</div>
        })
      : null;
  },
  info(context, text, icon) {
    return context.props.notificationSystem
      ? context.props.notificationSystem.addNotification({
          level: "info",
          position: "tr",
          title: icon ? <i className={`fa fa-${icon}`} /> : "",
          message: <div>{text}</div>
        })
      : null;
  },
  ongoing(context, text, icon) {
    return context.props.notificationSystem
      ? context.props.notificationSystem.addNotification({
          level: "info",
          position: "tr",
          dismissible: "none",
          autoDismiss: 0,
          title: icon ? <i className={`fa fa-${icon}`} /> : "",
          message: <div>{text}</div>
        })
      : null;
  },
  error(context, text) {
    return context.props.notificationSystem
      ? context.props.notificationSystem.addNotification({
          level: "error",
          position: "tr",
          dismissible: "both",
          autoDismiss: 5,
          title: <i className={`fa fa-close`} />,
          message: <div>{text}</div>
        })
      : null;
  },
  removeNotification(context, notification) {
    if (context.props.notificationSystem && notification)
      return context.props.notificationSystem.removeNotification(notification);
    else return null;
  }
};
