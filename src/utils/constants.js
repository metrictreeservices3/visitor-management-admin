import React from "react";
export const API_URL = process.env.REACT_APP_API_URL
  ? process.env.REACT_APP_API_URL
  : "https://api-visits.daddyspocket.com";
export const DATE_OPTIONS = {
  month: "short",
  year: "numeric",
  day: "numeric"
};
export const TIME_OPTIONS = {
  hour: "numeric",
  minute: "numeric",
  hour12: true
};
export const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const EMAIL_ERROR = (
  <small className="text-danger">
    Email is required and format should be <i>john@doe.com</i>.
  </small>
);

export const NAME_ERROR = (
  <small className="text-danger">Name is required.</small>
);
export const HOST_ERROR = (
  <small className="text-danger">Please choose a host.</small>
);

export const VISITOR_TIMELINE_TABLE_COLUMNS = [
  {
    Header: "Name",
    accessor: "name",
    filterable: true
  },
  {
    Header: "Date",
    accessor: "date",
    filterable: true
  },
  {
    Header: "In",
    accessor: "checkInTime",
    filterable: true
  },
  {
    Header: "Out",
    accessor: "checkOutTime",
    filterable: true
  },
  {
    Header: "Company",
    accessor: "company",
    filterable: true
  },
  {
    Header: "Actions",
    accessor: "actions",
    sortable: false,
    filterable: false
  }
];

export const EMPLOYEE_TIMELINE_TABLE_COLUMNS = [
  {
    Header: "Name",
    accessor: "name",
    filterable: true
  },
  {
    Header: "Date",
    accessor: "date",
    filterable: true
  },
  {
    Header: "In",
    accessor: "checkInTime",
    filterable: true
  },
  {
    Header: "Out",
    accessor: "checkOutTime",
    filterable: true
  },
  {
    Header: "Actions",
    accessor: "actions",
    sortable: false,
    filterable: false
  }
];

export const EMPLOYEES_TABLE_COLUMNS = [
  {
    Header: "Name",
    accessor: "name",
    filterable: true
  },
  {
    Header: "Role",
    accessor: "role",
    filterable: true
  },
  {
    Header: "Email",
    accessor: "email",
    filterable: true
  },
  {
    Header: "Phone",
    accessor: "phone",
    filterable: true
  },
  {
    Header: "Location",
    accessor: "location",
    filterable: true
  },
  {
    Header: "Actions",
    accessor: "actions",
    sortable: false,
    filterable: false
  }
];

export const COMMON_HEADER = access_token => {
  return {
    headers: {
      "x-access-token": access_token
    }
  };
};

export const TIMEZONES = [
  "( GMT ) GMT, Greenwich Mean Time",
  "( GMT+1:00 ) ECT, European Central Time",
  "( GMT+2:00 ) EET, Eastern European Time",
  "( GMT+2:00 ) ART, (Arabic) Egypt Standard Time",
  "( GMT+3:00 ) EAT, Eastern African Time",
  "( GMT+3:30) MET, Middle East Time",
  "( GMT+4:00 ) NET, Near East Time",
  "( GMT+5:00 ) PLT, Pakistan Lahore Time",
  "( GMT+5:30) IST, India Standard Time",
  "( GMT+6:00 ) BST, Bangladesh Standard Time",
  "( GMT+7:00 ) VST, Vietnam Standard Time",
  "( GMT+8:00 ) CTT, China Taiwan Time",
  "( GMT+9:00 ) JST, Japan Standard Time",
  "( GMT+9:30) ACT, Australia Central Time",
  "( GMT+10:00 ) AET, Australia Eastern Time",
  "( GMT+11:00 ) SST, Solomon Standard Time",
  "( GMT+12:00 ) NST, New Zealand Standard Time",
  "( GMT-11:00 ) MIT, Midway Islands Time",
  "( GMT-10:00 ) HST, Hawaii Standard Time",
  "( GMT-9:00 ) AST, Alaska Standard Time",
  "( GMT-8:00 ) PST, Pacific Standard Time",
  "( GMT-7:00 ) PNT, Phoenix Standard Time",
  "( GMT-7:00 ) MST, Mountain Standard Time",
  "( GMT-6:00 ) CST, Central Standard Time",
  "( GMT-5:00 ) EST, Eastern Standard Time",
  "( GMT-5:00 ) IET, Indiana Eastern Standard Time",
  "( GMT-4:00 ) PRT, Puerto Rico and US Virgin Islands Time",
  "( GMT-3:30) CNT, Canada Newfoundland Time",
  "( GMT-3:00 ) AGT, Argentina Standard Time",
  "( GMT-3:00 ) BET, Brazil Eastern Time",
  "( GMT-1:00 ) CAT, Central African Time"
];
