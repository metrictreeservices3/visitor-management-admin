import React from "react";
import Button from "../components/CustomButton/CustomButton.jsx";
import {
  API_URL,
  DATE_OPTIONS,
  TIME_OPTIONS,
  EMAIL_REGEX,
  EMAIL_ERROR,
  NAME_ERROR,
  HOST_ERROR,
  COMMON_HEADER
} from "./constants";
import axios from "axios";
import RichTextEditor from "react-rte";

import NotificationManager from "./Notifications";

export const logout = () => {
  localStorage.clear();
  window.location.assign("#/auth/login");
};

export const handleError = err => {
  console.error(err);
  let error_message = "";
  let error = err.response ? err.response.data : null;
  if (error && error.status) {
    switch (error.status) {
      case 401: {
        logout();
        error_message = "Session Expired. Please login again.";
        break;
      }
      case 400: {
        error_message = error.error;
        break;
      }
      case 500: {
        error_message = "Server Error";
        break;
      }
      default: {
        error_message = "Network Error";
        break;
      }
    }
  } else {
    error_message = "Something Happened";
  }
  return error_message;
};

export const getSignOutButton = (id, context) => {
  return (
    <div className="actions-right">
      <Button
        onClick={e => {
          signOut(id, context);
        }}
        bsStyle="primary"
        simple
        icon
      >
        Sign Out <i className="fa fa-times" />
      </Button>{" "}
    </div>
  );
};

export const getSignInButton = (id, context, callback) => {
  return (
    <div className="actions-right">
      <Button
        onClick={e => {
          signIn(id, context, callback);
        }}
        bsStyle="primary"
        simple
        icon
      >
        Sign In <i className="fa fa-sign-in" />
      </Button>{" "}
    </div>
  );
};

export const signOut = (id, context) => {
  const access_token = localStorage.getItem("access_token");
  if (!context.state.is_signingout) {
    context.setState({
      is_signingout: true
    });
    axios
      .get(`${API_URL}/session/checkout/${id}`, COMMON_HEADER(access_token))
      .then(response => {
        getData(context);
        context.setState({
          is_signingout: false
        });
      })
      .catch(error => {
        NotificationManager.error(context, handleError(error));
        context.setState({
          is_signingout: false
        });
      });
  }
};

export const signIn = (id, context, callback) => {
  const access_token = localStorage.getItem("access_token");
  if (!context.state.is_signingout) {
    context.setState({
      is_signingout: true
    });
    axios
      .post(`${API_URL}/session/checkin/${id}`, new FormData(), {
        headers: {
          "x-access-token": access_token,
          "content-type": "multipart/form-data"
        }
      })
      .then(response => {
        if (callback) callback();
        getData(context);
        context.setState({
          is_signingout: false
        });
      })
      .catch(error => {
        NotificationManager.error(context, handleError(error));
        context.setState({
          is_signingout: false
        });
      });
  }
};

export const getData = (context, callback) => {
  const access_token = localStorage.getItem("access_token");
  let loader = NotificationManager.ongoing(context, "Updating.", "spinner");
  axios
    .get(`${API_URL}/session/`, COMMON_HEADER(access_token))
    .then(response => {
      if (callback) callback();
      NotificationManager.removeNotification(context, loader);
      context.setState({
        employees_in: response.data.data.overview.employeesIn,
        employees_out: response.data.data.overview.employeesOut,
        visitors_in: response.data.data.overview.visitorsIn,
        pre_registered: response.data.data.overview.preregistered,
        employeesData: response.data.data.employees.map((employee, key) => {
          return {
            id: key,
            name: employee.userDetails.name.trim(),
            checkInTime: new Date(employee.checkInTime).toLocaleTimeString(
              "en-Us",
              TIME_OPTIONS
            ),
            checkOutTime: employee.checkOutTime
              ? new Date(employee.checkOutTime).toLocaleTimeString(
                  "en-Us",
                  TIME_OPTIONS
                )
              : null,
            date: new Date(employee.checkInTime).toLocaleDateString(
              "en-Us",
              DATE_OPTIONS
            ),
            actions: employee.checkOutTime
              ? getSignInButton(employee.user, context)
              : getSignOutButton(employee.id, context)
          };
        }),
        visitorsData: response.data.data.visitors.map((visitor, key) => {
          return {
            id: key,
            name: visitor.userDetails.name
              ? visitor.userDetails.name.trim()
              : "",
            checkInTime: new Date(visitor.checkInTime).toLocaleTimeString(
              "en-Us",
              TIME_OPTIONS
            ),
            checkOutTime: visitor.checkOutTime
              ? new Date(visitor.checkOutTime).toLocaleTimeString(
                  "en-Us",
                  TIME_OPTIONS
                )
              : null,
            date: new Date(visitor.checkInTime).toLocaleDateString(
              "en-Us",
              DATE_OPTIONS
            ),
            company: visitor.userDetails.companyName.trim(),
            actions: visitor.checkOutTime
              ? getSignInButton(visitor.user, context)
              : getSignOutButton(visitor.id, context)
          };
        })
      });
    })
    .catch(error => {
      NotificationManager.error(context, handleError(error));
    });
};

export const getVisitorsList = context => {
  const access_token = localStorage.getItem("access_token");
  axios
    .get(`${API_URL}/visitor/`, COMMON_HEADER(access_token))
    .then(response => {
      let visitorsList = [];
      response.data.forEach(visitor => {
        visitorsList.push({
          label:
            visitor && visitor.name && visitor.companyName
              ? `${visitor.name.trim()}, ${visitor.companyName.trim()}`
              : "",
          value: visitor.id,
          id: visitor.id,
          name: visitor.name,
          companyName: visitor.companyName,
          email: visitor.email,
          location: visitor.location
        });
      });
      context.setState({
        visitorsList: visitorsList
      });
    })
    .catch(error => {
      NotificationManager.error(context, handleError(error));
    });
};

export const getAllEmployees = context => {
  const access_token = localStorage.getItem("access_token");
  axios
    .get(`${API_URL}/employee/all`, COMMON_HEADER(access_token))
    .then(response => {
      let employeesList = [];
      response.data.users.forEach(employee => {
          employeesList.push({
            label: employee.name.trim(),
            value: employee.id,
            name: employee.name
          });
      });
      context.setState({
        employeesList: employeesList
      
      });
    })
    .catch(error => {
      NotificationManager.error(context, handleError(error));
    });
};


export const getEmployeesList = context => {
  const access_token = localStorage.getItem("access_token");
  axios
    .get(`${API_URL}/employee/`, COMMON_HEADER(access_token))
    .then(response => {
      let employeesList = [];
      response.data.users.forEach(employee => {
        if (employee.activeSessionId)
          employeesList.push({
            label: employee.name.trim(),
            value: employee.id,
            name: employee.name
          });
      });
      context.setState({
        employeesList: employeesList
      });
    })
    .catch(error => {
      NotificationManager.error(context, handleError(error));
    });
};

export const visitorDataIsNotValid = context => {
  if (context.state.name && context.state.name !== "") {
    context.setState({
      nameError: null
    });
  } else {
    context.setState({
      nameError: NAME_ERROR
    });
    return true;
  }
  if (context.state.email && context.state.email !== "") {
    if (!EMAIL_REGEX.test(context.state.email)) {
      context.setState({
        emailError: EMAIL_ERROR
      });
      return true;
    } else {
      context.setState({
        emailError: null
      });
    }
  } else {
    context.setState({
      emailError: EMAIL_ERROR
    });
    return true;
  }
  if (
    context.state.selectedEmployeeId &&
    context.state.selectedEmployeeId !== ""
  ) {
    context.setState({
      hostError: null
    });
  } else {
    context.setState({
      hostError: HOST_ERROR
    });
    return true;
  }
  return false;
};

export const getSettings = (context, settings) => {
  const access_token = localStorage.getItem("access_token");
  let loader = NotificationManager.ongoing(context, "Updating.", "spinner");
  axios
    .post(
      `${API_URL}/settings/`,
      {
        filters: settings
      },
      COMMON_HEADER(access_token)
    )
    .then(response => {
      NotificationManager.removeNotification(context, loader);
      switch (settings[0]) {
        case "screenDesign": {
          context.setState({
            companyLogo: response.data.data.screenDesign.companyLogo,
            accentColor: response.data.data.screenDesign.accentColor,
            homeScreenButtonColor:
              response.data.data.screenDesign.homeScreenButtonColor,
            buttonTextColor: response.data.data.screenDesign.buttonTextColor,
            employeeSwipeButtonColor:
              response.data.data.screenDesign.employeeSwipeButtonColor,
            visitor_in_button_text:
              response.data.data.screenDesign.visitorButtonText.in,
            visitor_out_button_text:
              response.data.data.screenDesign.visitorButtonText.out,
            employeeSwipeButtonText:
              response.data.data.screenDesign.employeeSwipeButtonText,
            backgroundImages: response.data.data.screenSaver.map(item => {
              return {
                id: item["id"],
                url: item["url"].startsWith("http")
                  ? item["url"]
                  : `${API_URL}/${item["url"]}`
              };
            })
          });
          break;
        }
        case "visitorAgreement": {
          let text = response.data.data.visitorAgreement.text;
          if (text && text !== "") {
            context.setState({
              initialValue: RichTextEditor.createValueFromString(text, "html"),
              value: RichTextEditor.createValueFromString(text, "html")
            });
          }
          break;
        }
        default: {
        }
      }
    })
    .catch(error => {
      NotificationManager.error(context, handleError(error));
    });
};

export const employeeDataIsNotValid = context => {
  if (context.state.name && context.state.name !== "") {
    context.setState({
      nameError: null
    });
  } else {
    context.setState({
      nameError: NAME_ERROR
    });
    return true;
  }
  if (context.state.email && context.state.email !== "") {
    if (!EMAIL_REGEX.test(context.state.email)) {
      context.setState({
        emailError: EMAIL_ERROR
      });
      return true;
    } else {
      context.setState({
        emailError: null
      });
    }
  } else {
    context.setState({
      emailError: EMAIL_ERROR
    });
    return true;
  }
  return false;
};

export const parseColor = color => {
  if (!color) return null;
  if (color[0] === "#") return hex2rgba(color.replace("#", "")).rgb;
  let colors = color
    .replace(/rgba\(/, "")
    .replace(/\)/, "")
    .split(",");
  return {
    r: colors[0].trim(),
    g: colors[1].trim(),
    b: colors[2].trim(),
    a: colors[3].trim()
  };
};

export const hex2rgba = hexVal => {
  // turn 3-HEX to 6-HEX
  if (hexVal.length === 3) {
    hexVal =
      hexVal[0] + hexVal[0] + hexVal[1] + hexVal[1] + hexVal[2] + hexVal[2];
  }

  // extracting the hex values for RGB
  var red = hexVal.substr(0, 2),
    green = hexVal.substr(2, 2),
    blue = hexVal.substr(4, 2);

  // converting in decimal values
  var red10 = parseInt(red, 16),
    green10 = parseInt(green, 16),
    blue10 = parseInt(blue, 16);

  // stitching it together
  var rgb = red10 + "," + green10 + "," + blue10;

  // the final rgba CSS ouptut
  let rgba = "rgba(" + rgb + ",1)";
  console.log(rgba);
  return {
    rgb: {
      r: red10.toString(),
      g: green10.toString(),
      b: blue10.toString(),
      a: "1"
    },
    rgbString: rgba
  };
};
export const evacuate = (context, type, callback) => {
  const access_token = localStorage.getItem("access_token");
  let loader = NotificationManager.ongoing(
    context,
    "Please wait...",
    "spinner"
  );
  axios
    .get(
      `${API_URL}/session/evacuate?type=${type}`,
      COMMON_HEADER(access_token)
    )
    .then(response => {
      NotificationManager.removeNotification(context, loader);
      callback();
    })
    .catch(error => {
      NotificationManager.error(context, handleError(error));
    });
};
export const getPlans = context => {
  const access_token = localStorage.getItem("access_token");
    axios
      .get(`${API_URL}/subscription`,  COMMON_HEADER(access_token))
      .then(response => {
        context.setState({
          plans: response.data.data
        });
      })
      .catch(e => console.log(e));
}
export const printTable = () => {
  window.print();
};

export const getCurrentPlan = context => {
  const access_token = localStorage.getItem("access_token");
    axios
      .get(`${API_URL}/subscription/active`,COMMON_HEADER(access_token))
      .then(response => {
        context.setState({
          currentPlan: response.data.data.UserPlans[0].name,
          subscribedOn: response.data.data.UserPlans[0].Subscription.subscribedOn,
          expiresOn: response.data.data.UserPlans[0].Subscription.expiresOn,
          employeeLimit: response.data.data.UserPlans[0].employeeLimit,
          deviceLimit: response.data.data.UserPlans[0].deviceLimit,
        });
      })
      .catch(e => console.log(e));
}
