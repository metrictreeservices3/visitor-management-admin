import Dashboard from "../views/Dashboard/Dashboard.jsx";
import Employees from "../views/Employees.jsx";
import Visitors from "../views/Visitors.jsx";
import Preregister from "../views/Preregister.jsx";
import Checkin from "../views/Checkin.jsx";
import Branding from "../views/Settings/Branding.jsx";
import AdminRoles from "../views/Settings/AdminRoles.jsx";
import Notifications from "../views/Settings/Notifications.jsx";
import PhotoID from "../views/Settings/PhotoID.jsx";
import VisitorsSettings from "../views/Settings/VisitorsSettings.jsx";
import VisitorsAgreement from "../views/Settings/VisitorsAgreement.jsx";
import iPads from "../views/Settings/iPads.jsx";
import Company from "../views/Account/Company.jsx";
import Pricing from "../views/Account/Pricing.jsx";
import Billing from "../views/Account/Billing.jsx";
// import SMS from "../views/Account/SMS.jsx";

// import pagesRoutes from "./auth.jsx";
import { logout } from "../utils/commons.js";

// var pages = [].concat(pagesRoutes);

var dashboardRoutes = [
  {
    path: "/checkin",
    name: "Checkin",
    icon: "pe-7s-check",
    component: Checkin
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "pe-7s-graph",
    component: Dashboard
  },
  {
    path: "/employees",
    name: "Employees",
    icon: "pe-7s-users",
    component: Employees
  },
  {
    path: "/visitors",
    name: "Visitors",
    icon: "pe-7s-id",
    component: Visitors
  },
  {
    path: "/preregister",
    name: "Preregister",
    icon: "pe-7s-add-user",
    component: Preregister
  },

  {
    collapse: true,
    path: "/settings",
    name: "Settings",
    state: "openSettings",
    icon: "pe-7s-tools",
    views: [
      {
        path: "/settings/branding",
        name: "Branding",
        mini: "pe-7s-display1",
        component: Branding
      },
      // {
      //   path: "/settings/admin-roles",
      //   name: "Admin Roles",
      //   mini: "pe-7s-look",
      //   component: AdminRoles
      // },
      // {
      //   path: "/settings/photo-id",
      //   name: "Photo & ID",
      //   mini: "pe-7s-photo",
      //   component: PhotoID
      // },
      // {
      //   path: '/settings/visitors-fields',
      //   name: 'Visitors Fields',
      //   mini: 'VF',
      //   component: VisitorsFields
      // },
      {
        path: "/settings/visitors-agreement",
        name: "Visitors Agreement",
        mini: "pe-7s-news-paper",
        component: VisitorsAgreement
      },
      // {
      //   path: "/settings/visitors-settings",
      //   name: "Visitors Settings",
      //   mini: "pe-7s-tools",
      //   component: VisitorsSettings
      // },
      // {
      //   path: "/settings/notifications",
      //   name: "Notifications",
      //   mini: "pe-7s-comment",
      //   component: Notifications
      // },
      // {
      //   path: '/settings/employee',
      //   name: 'Employee Settings',
      //   mini: 'ES',
      //   component: EmployeesSettings
      // },
      {
        path: "/settings/ipads",
        name: "iPads",
        mini: "fa fa-tablet",
        component: iPads
      }
    ]
  },
  {
    collapse: true,
    path: "/account",
    name: "Account",
    state: "openAccount",
    icon: "pe-7s-credit",
    views: [
      // {
      //   path: "/account/company",
      //   name: "Company",
      //   mini: "pe-7s-rocket",
      //   component: Company
      // },
      {
        path: "/account/pricing",
        name: "Pricing",
        mini: "pe-7s-graph2",
        component: Pricing
      },
      {
        path: "/account/billing",
        name: "Billing",
        mini: "pe-7s-cash",
        component: Billing
      },
      // {
      //   path: "/account/sms",
      //   name: "SMS",
      //   mini: "pe-7s-chat",
      //   component: SMS
      // }
    ]
  },
  {
    path: "/auth/login",
    name: "Logout",
    icon: "pe-7s-power",
    action: logout,
    component: Dashboard
  },

  { redirect: true, path: "/", pathTo: "/dashboard", name: "Dashboard" }
];
export default dashboardRoutes;
