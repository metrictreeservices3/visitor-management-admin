// import Pages from "../layouts/Pages/Pages.jsx";
import Pages from "../layouts/Pages/Pages.jsx"
import Dashboard from "../layouts/Dashboard/Dashboard.jsx";

var indexRoutes = [
  { path: "/auth", name: "Auth", component: Pages },
  { path: "/", name: "Dashboard", component: Dashboard }
];

export default indexRoutes;
