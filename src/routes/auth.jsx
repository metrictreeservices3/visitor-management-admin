import LoginPage from "../views/Pages/LoginPage.jsx";
import ForgotPasswordPage from "../views/Pages/ForgotPasswordPage.jsx";
import ResetPasswordPage from "../views/Pages/ResetPasswordPage.jsx";
var pagesRoutes = [
  {
    path: "/auth/login",
    name: "Login Page",
    mini: "LP",
    component: LoginPage
  },
  {
    path: "/auth/forgot_password",
    name: "Forgot Password Page",
    mini: "FPP",
    component: ForgotPasswordPage
  }, {
    path: "/auth/reset_password/:token",
    name: "Reset Password Page",
    mini: "RPP",
    component: ResetPasswordPage
  }
];

export default pagesRoutes;
