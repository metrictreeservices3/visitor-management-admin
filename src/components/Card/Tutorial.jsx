import React, { Component } from "react";

class Tutorial extends Component {
  render() {
    return (
      <div className="card card-tutorial">
         <div className="header">
         <h3>{this.props.step}</h3>
        </div> 
        <div className="content">
        {this.props.content}</div>
        <div className="row">
            <div className="col-6">
                <h3>{this.props.heading}</h3>
                <h4>{this.props.stps}</h4>
            </div>
            <div className="col-6">
                <img src={this.props.image}></img>
            </div>
        </div>
      </div>
    );
  }
}

export default Tutorial;
