import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

import PagesHeader from "../../components/Header/PagesHeader.jsx";
import NotificationSystem from "react-notification-system";
// style for notifications
import { style } from "../../variables/Variables.jsx";

// dinamically create pages routes
// import pagesRoutes from "routes/auth.jsx";
import pagesRoutes from "../../routes/auth.jsx";


import bgImage from "../../assets/img/full-screen-image-2.jpg";

class Pages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _notificationSystem: null
    };
  }
  componentDidMount() {
    this.setState({
      _notificationSystem: this.refs.notificationSystem
    });
  }
  getPageClass() {
    var pageClass = "";
    switch (this.props.location.pathname) {
      case "/auth/login":
        pageClass = " login-page";
        break;
      case "/auth/forgot_password":
        pageClass = " lock-page";
        break;
      default:
        pageClass = "";
        break;
    }
    return pageClass;
  }
  componentWillMount() {
    if (document.documentElement.className.indexOf("nav-open") !== -1) {
      document.documentElement.classList.toggle("nav-open");
    }
  }
  render() {
    return (
      <div>
        <NotificationSystem ref="notificationSystem" style={style} />
        <PagesHeader />
        <div className="wrapper wrapper-full-page">
          <div
            className={"full-page" + this.getPageClass()}
            data-color="black"
            data-image={bgImage}
          >
            <div className="content">
              <Switch>
                {pagesRoutes.map((prop, key) => {
                  return (
                    <Route
                      path={prop.path}
                      render={routeProps => (
                        <prop.component
                          {...routeProps}
                          notificationSystem={this.state._notificationSystem}
                        />
                      )}
                      key={key}
                    />
                  );
                })}
              </Switch>
            </div>
            <div
              className="full-page-background"
              style={{ backgroundImage: "url(" + bgImage + ")" }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Pages;
