import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
// this is used to create scrollbars on windows devices like the ones from apple devices
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// react component that creates notifications (like some alerts with messages)
import NotificationSystem from "react-notification-system";

import Sidebar from "../../components/Sidebar/Sidebar.jsx";
import Header from "../../components/Header/Header.jsx";
import Footer from "../../components/Footer/Footer.jsx";

// dinamically create dashboard routes
import dashboardRoutes from "../../routes/dashboard.jsx";

// style for notifications
import { style } from "../../variables/Variables.jsx";

var ps;

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _notificationSystem: null
    };
  }
  componentDidMount() {
    this.setState({
      _notificationSystem: this.refs.notificationSystem
    });
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.refs.mainPanel);
    }
    document.addEventListener("keydown", function(zEvent) {
      if (
        (zEvent.ctrlKey && zEvent.key === "a") ||
        (zEvent.ctrlKey && zEvent.keyCode === 49)
      ) {
        window.location.assign(`#/checkin`);
      } else if (
        (zEvent.ctrlKey && zEvent.key === "Enter") ||
        (zEvent.ctrlKey && zEvent.key === "b") ||
        (zEvent.ctrlKey && zEvent.keyCode === 50)
      ) {
        window.location.assign(`#/dashboard`);
      } else if (
        (zEvent.ctrlKey && zEvent.key === "c") ||
        (zEvent.ctrlKey && zEvent.keyCode === 51)
      ) {
        window.location.assign(`#/employees`);
      } else if (
        (zEvent.ctrlKey && zEvent.key === "d") ||
        (zEvent.ctrlKey && zEvent.keyCode === 52)
      ) {
        window.location.assign(`#/visitors`);
      } else if (
        (zEvent.ctrlKey && zEvent.key === "e") ||
        (zEvent.ctrlKey && zEvent.keyCode === 53)
      ) {
        window.location.assign(`#/preregister`);
      }
    });
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
    }
  }
  componentDidUpdate(e) {
    if (navigator.platform.indexOf("Win") > -1) {
      setTimeout(() => {
        ps.update();
      }, 350);
    }
    if (e.history.action === "PUSH") {
      document.documentElement.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
      this.refs.mainPanel.scrollTop = 0;
    }
    if (
      window.innerWidth < 993 &&
      e.history.action === "PUSH" &&
      document.documentElement.className.indexOf("nav-open") !== -1
    ) {
      document.documentElement.classList.toggle("nav-open");
    }
  }
  componentWillMount() {
    if (document.documentElement.className.indexOf("nav-open") !== -1) {
      document.documentElement.classList.toggle("nav-open");
    }
  }

  render() {
    return (
      <div className="wrapper">
        <NotificationSystem ref="notificationSystem" style={style} />
        <Sidebar {...this.props} />
        <div
          className={
            "main-panel" +
            (this.props.location.pathname === "/maps/full-screen-maps"
              ? " main-panel-maps"
              : "")
          }
          ref="mainPanel"
        >
          <Header {...this.props} />
          <Switch>
            {dashboardRoutes.map((prop, key) => {
              if (prop.collapse) {
                return prop.views.map((prop, key) => {
                  if (prop.name === "Notifications") {
                    return (
                      <Route
                        path={prop.path}
                        key={key}
                        render={routeProps => (
                          <prop.component
                            {...routeProps}
                            notificationSystem={this.state._notificationSystem}
                          />
                        )}
                      />
                    );
                  } else {
                    return (
                      <Route
                        path={prop.path}
                        // component={prop.component}
                        render={routeProps => (
                          <prop.component
                            {...routeProps}
                            notificationSystem={this.state._notificationSystem}
                          />
                        )}
                        key={key}
                      />
                    );
                  }
                });
              } else {
                if (prop.redirect)
                  return (
                    <Redirect from={prop.path} to={prop.pathTo} key={key} />
                  );
                else
                  return (
                    <Route
                      path={prop.path}
                      // component={prop.component}
                      render={routeProps => (
                        <prop.component
                          {...routeProps}
                          notificationSystem={this.state._notificationSystem}
                        />
                      )}
                      key={key}
                    />
                  );
              }
            })}
          </Switch>
          <Footer fluid />
        </div>
      </div>
    );
  }
}

export default Dashboard;
